#!/bin/sh
# abort this script on any error
set -e

./.installreq.sh
./.builddry.sh

git pull
cd ..
mkdir heXon-build
cd heXon-build
qmake ../heXon/heXon.pro
sudo make install
./.postinstall.sh
cd ..
rm -rf heXon-build
