## Used licenses
_[Public domain](https://creativecommons.org/publicdomain/mark/1.0/), [CC0](https://creativecommons.org/publicdomain/zero/1.0/), [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/), [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)_

## Assets by type, author and license

### Various

##### Author irrelevant
- CC0
    + Docs/*
    + Screenshots/*
    + Resources/Materials/*
    + Resources/UI/*.xml 
    + Resources/Shaders/*
    + Resources/Techniques/*
    + Resources/RenderPaths/*
    + Resources/PostProcess/*
    + Resources/Textures/Ramp.*
    + Resources/Textures/Spot.*
    + Resources/Textures/*.xml
    
### Music

#### Alien Chaos
- CC-BY 4.0
    + Resources/Music/Alien Chaos - Disorder.ogg [| Source |](https://www.ektoplazm.com/free-music/alien-chaos-discovering-the-code)

#### Biomekanik
- CC-BY 4.0
    + Resources/Music/Biomekanik - Symphony of Noises.ogg [| Source |](https://ektoplazm.com/free-music/biomekanik-cinematech)

#### Modanung
- CC-BY-SA 4.0
    + Resources/Music/Modanung - BulletProof MagiRex.ogg

### Samples

#### Modanung
- CC-BY-SA 4.0
    + Resources/Samples/*
    
### 2D

#### j4p4n
- Public domain
    + Resources/Textures/SunStone.dds [| Source |](https://openclipart.org/detail/166991/aztec-calender)
    
#### Modanung
- CC-BY-SA 4.0 
    + hexon.svg
    + Resources/icon.png
    + Vector/*
    + Resources/UI/*.png
    + GIMP/*
    + Resources/Textures/BackgroundTile.dds
    + Resources/Textures/Blood.dds
    + Resources/Textures/BloodDissolve.dds
    + Resources/Textures/BloodDrop.dds
    + Resources/Textures/BloodSplat.dds
    + Resources/Textures/Bullet.dds
    + Resources/Textures/Controller.dds
    + Resources/Textures/CoreGradient.dds
    + Resources/Textures/Cubemap_back.dds
    + Resources/Textures/Cubemap_bottom.dds
    + Resources/Textures/Cubemap_front.dds
    + Resources/Textures/Cubemap_left.dds
    + Resources/Textures/Cubemap_right.dds
    + Resources/Textures/Cubemap_top.dds
    + Resources/Textures/Digit.dds
    + Resources/Textures/DrainDiffuse.dds
    + Resources/Textures/DrainEmissive.dds
    + Resources/Textures/DrainNormal.dds
    + Resources/Textures/DrainSpecular.dds
    + Resources/Textures/Explosion.dds
    + Resources/Textures/Flash.dds
    + Resources/Textures/Mason.dds
    + Resources/Textures/Mirage.dds
    + Resources/Textures/Ram.dds
    + Resources/Textures/RazorSpriteDiffuse.dds
    + Resources/Textures/RazorSpriteEmissive.dds
    + Resources/Textures/RazorSpriteNormal.dds
    + Resources/Textures/Repel.dds
    + Resources/Textures/Shield.dds
    + Resources/Textures/Shine.dds
    + Resources/Textures/SmallBubble.dds
    + Resources/Textures/Spire_alpha.dds
    + Resources/Textures/Spire_emission.dds
    + Resources/Textures/Digit.png
    + Resources/Textures/Ram.png

### 3D

#### Modanung
- CC-BY-SA 4.0 
    + Blends/*
    + Resources/Models/*