![heXon logo](Docs/Guide/images/heXonBanner.png)

[![pipeline status](https://gitlab.com/luckeyproductions/hexon/badges/master/pipeline.svg)](https://gitlab.com/luckeyproductions/hexon/-/commits/master) [![coverage report](https://gitlab.com/luckeyproductions/hexon/badges/master/coverage.svg)](https://gitlab.com/luckeyproductions/hexon/-/commits/master)

#### Contents

- [Summary](#summary)
<!--- [Installation](#installation)-->
- [Play guide](#play-guide)

## Summary

heXon is a free and open source twin-stick-shooter created using the [Dry](http://urho3d.github.io) game engine.

To score high you must fly well, avoiding the Notyous and destroying them with your Whack-o-Slack blast battery. Your firepower can be increased by collecting five apples. Picking up five hearts in a row will charge your shield.  
All edges of the hexagonal arena are connected to its opposite like portals, making for a puzzlingly dangerous playing field that might take some experience to wrap your head around.

## Play guide
[Open **heXoGuide**](Docs/Guide/heXoGuide_EN.md)

### Screenshots
[![heXon screenshot](Screenshots/Screenshot_Thu_Dec_22_05_25_44_2016.png)](Screenshots/Screenshot_Thu_Dec_22_05_25_44_2016.png)
[![heXon screenshot](Screenshots/Screenshot_Sun_Jun__5_03_02_18_2016.png)](Screenshots/Screenshot_Sun_Jun__5_03_02_18_2016.png)

<!--
## Installation
### Debian-based linux distros
#### Pre-built

Visit [heXon's itch.io page](http://luckeyproductions.itch.io/hexon) and hit **Download Now**. Or download heXon through the [itch app](https://itch.io/app).


#### Compiling from source

If the binary is not working you may try compiling by running this line in a terminal:

```
git clone https://github.com/LucKeyProductions/heXon; cd heXon; ./install.sh; cd ..; rm -rf heXon
```
-->


