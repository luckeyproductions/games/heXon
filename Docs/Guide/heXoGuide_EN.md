
![](images/heXonLogo_trans256.png)

Welcome to the ultimate guide to heXon. Herein you will find everything you need to know before taking your first dive.

## The Kl&aring; Mk.10

![](images/Kla.png)
### Features
The Kl&aring; mark two is the very latest and finest in triple cutting edge FOSS hyper-dimensional nasty shape-invader whacking technology.
 The Kl&aring; is equipped with a Whack-O-Slack Blast Battery which requires exactly five Golden Apples to increase its firepower.

### Controls

The left stick flies the Kl&aring;, the right one aims and fires its Whack-O-Slack Blast Battery.

## Enemies

These Notyous happen to be invading this shape you claimed, threatening your very existence while at the same time
 feeding your possibly high score.


![](images/Razor.png)
### Razors

Mostly harmless in small numbers. Don't fly into them though. Razors move faster and cut deeper when they are damaged. Destroying one is rewarded with 5 points.


![](images/Spire.png)
### Spires

These sturdy towers launch player-seeking foo fighters that should be evaded if turning to dust isn't your idea of having a good time. Spires are tough, but can be destroyed. Obliterating one will earn you 10 points.
Their fire rate increases with their aggravation, so be sure to finish them off properly.

### Masons

"_Spin, spin, hurly!_" Seems to be the operational basis of these brickies. Do not let their simple programming fool you when it comes to the pack of their punch. Masons may be sturdy, but their destruction _is_ worth 42 points.
	
## Pickups

Attention! Apples and Hearts are not counted simultaneously.

![](images/Apple.png)
### Apple

Another way of feeding your score is through collecting them apples. Each provides a whopping bunch of 23 points. Collect five apples in a row to get a weapon upgrade.


![](images/Heart.png)
### Heart

Love is what keeps us alive. One of these Hearts heals half your life. Collect five hearts in a row to acquire full health and a shield upgrade.
 The shield can only take a certain amount of damage and protects mostly against energy weapons.


![](images/ChaoBall.png)
### ChaoBall

Evokes dense localized Chaos. Sheer luck or skill can haul in quite some points when applied to this spiky blackness.
 When touched, a flash of Chaos turns all close enemies into ChaoMines. These mines can be pushed into busy spots and will instantly dismantle all enemies within a small radius. Potentially leaving you with a lot of points.
