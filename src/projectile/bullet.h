/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BULLET_H
#define BULLET_H

#include "../sceneobject.h"

class Bullet: public SceneObject
{
    DRY_OBJECT(Bullet, SceneObject);

    friend class Ship;
    friend class SpawnMaster;

public:
    static void RegisterObject(Context* context);
    Bullet(Context* context);

    void Start() override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void Set(const Vector3& position, const int colorSet, const Vector3& direction, const Vector3& force, float damage);

    int GetPlayerID() const noexcept { return colorSet_; }

protected:
    SharedPtr<RigidBody> rigidBody_;

private:
    void HitCheck(const float timeStep);
    void Disable() override;

    int colorSet_;
    float age_;
    float timeSinceHit_;
    float lifeTime_;
    bool fading_;
    float damage_;

    static HashMap<int, StaticModelGroup*> bulletGroups_;
};

#endif // BULLET_H
