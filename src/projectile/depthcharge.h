/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DEPTHCHARGE_H
#define DEPTHCHARGE_H

#include "../sceneobject.h"

class DepthCharge: public SceneObject
{
    DRY_OBJECT(DepthCharge, SceneObject);

public:
    DepthCharge(Context* context);

    void Start() override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;

    virtual void Set(const Vector3& position, const Vector3& direction, int colorSet);
    void SetCountdown(float count) { countdown_ = count; }
    void Detonate();

    int GetColorSet() const { return colorSet_; }

private:
    void HandleNodeCollision(StringHash, VariantMap& eventData);

    StaticModel* model_;
    RigidBody* rigidBody_;
    CollisionShape* collider_;
    float countdown_;
    int colorSet_;
};

#endif // DEPTHCHARGE_H
