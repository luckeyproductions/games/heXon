/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../enemy/mason.h"
#include "../enemy/spire.h"
#include "../environment/arena.h"
#include "../fx/hitfx.h"
#include "../fx/phaser.h"
#include "../player/ship.h"
#include "chaomine.h"
#include "seeker.h"

#include "brick.h"

void Brick::RegisterObject(Context* context)
{
    context->RegisterFactory<Brick>();
}

Brick::Brick(Context* context): SceneObject(context),
    rigidBody_{ nullptr },
    trigger_{ nullptr },
    particleEmitter_{ nullptr },
    spikeMaterial_{ nullptr },
    damage_{ 3.4f },
    traveled_{}
{
    SetUpdateEventMask(USE_UPDATE | USE_FIXEDPOSTUPDATE);
}

void Brick::Start()
{
    SceneObject::Start();

//    MC->arena_->AddToAffectors(node_);

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(2.3f);
    rigidBody_->SetLinearDamping(.23f);
    rigidBody_->SetTrigger(true);
    rigidBody_->SetLinearFactor(Vector3::ONE - Vector3::UP);

    trigger_ = node_->CreateComponent<CollisionShape>();
    trigger_->SetBox(Vector3{ .666f, 3.4f, .23f });

    Node* spikeNode{ node_->CreateChild("Spike")};
    StaticModel* spikeModel{ spikeNode->CreateComponent<StaticModel>() };
    spikeModel->SetModel(RES(Model, "Models/Spike.mdl"));
    spikeMaterial_ = RES(Material, "Materials/Spike.xml")->Clone();
    spikeModel->SetMaterial(spikeMaterial_);

    Node* particleNode{ node_->CreateChild("Particles") };
    particleNode->SetPosition(Vector3::UP * .42f);
    particleEmitter_ = particleNode->CreateComponent<ParticleEmitter>();
    particleEmitter_->SetEffect(RES(ParticleEffect, "Particles/Brick.xml")->Clone());

    Light* light{ node_->CreateComponent<Light>() };
    light->SetRange(2.3f);
    light->SetBrightness(3.4f);
    light->SetColor(Color::WHITE);
}

void Brick::Set(const Vector3& position, const Vector3& direction, bool super)
{
    SceneObject::Set(position);

    damage_ = 3.4f + 1.7f * super;

    traveled_ = 0.f;

    rigidBody_->ResetForces();
    rigidBody_->SetLinearVelocity(Vector3::ZERO);

    particleEmitter_->RemoveAllParticles();
    particleEmitter_->SetEmitting(true);

    node_->SetScale(1.f + .2f * super);
    node_->LookAt(position + direction);
    particleEmitter_->GetEffect()->SetMinDirection(direction + Vector3::UP);
    particleEmitter_->GetEffect()->SetMaxDirection(direction + Vector3::UP);

    rigidBody_->ApplyImpulse(direction * (123.f + 42.f * super));

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Brick, HandleTriggerStart));
}

void Brick::FixedPostUpdate(float /*timeStep*/)
{
    float growth{ Clamp(traveled_, .1f, 1.f) };
    trigger_->SetBox(Vector3{ .666f, 3.4f, 4.2f * growth }, Vector3::BACK * 2.3f * growth);
}

void Brick::HandleTriggerStart(StringHash /*eventType*/, VariantMap& /*eventData*/)
{

    if (!node_->IsEnabled())
        return;

    PODVector<RigidBody*> collidingBodies{};
    rigidBody_->GetCollidingBodies(collidingBodies);

    for (unsigned i{ 0 }; i < collidingBodies.Size(); ++i)
    {
        RigidBody* collider{ collidingBodies[i] };
        Node* collidingNode{ collider->GetNode() };

        if (collidingNode->HasComponent<Ship>())
        {
            Ship* ship{ collidingNode->GetComponent<Ship>() };
            if (!ship->IsDiving())
            {
                ship->Hit(damage_, false);
                collider->ApplyImpulse(rigidBody_->GetLinearVelocity() * .5f);

                Disable();
            }
        }
        else if (collidingNode->HasComponent<ChaoMine>())
        {
            collidingNode->GetComponent<ChaoMine>()->Hit(damage_, 0);
        }
        else if (Spire* spire = collider->GetNode()->GetComponent<Spire>())
        {
            if (node_->GetDirection().ProjectOntoAxis((spire->GetWorldPosition() - node_->GetPosition()).Normalized()) > 0.f)
            {
                spire->Shoot(false)->SetLinearVelocity(rigidBody_->GetLinearVelocity() * .23f);
                Disable();
            }
        }
    }
}

void Brick::Disable()
{
//    SPAWN->Create<HitFX>()
//            ->Set(node_->GetPosition(), 0, false);
    Phaser* phaser{ SPAWN->Create<Phaser>() };
    phaser->Set(RES(Model, "Models/Spike.mdl"), GetWorldPosition(), rigidBody_->GetLinearVelocity() * .23f, false, false);

    SceneObject::Disable();

    particleEmitter_->GetNode()->SetEnabled(true);
    particleEmitter_->SetEmitting(false);
}

void Brick::Update(float timeStep)
{
    spikeMaterial_->SetShaderParameter("MatEmissiveColor", Color{ Random(.23f, 1.f), .666f, Random(.666f, 1.f) });
    traveled_ += rigidBody_->GetLinearVelocity().Length() * timeStep;
}
