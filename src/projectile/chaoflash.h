/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CHAOFLASH_H
#define CHAOFLASH_H

#include "../sceneobject.h"

class ChaoFlash: public SceneObject
{
    DRY_OBJECT(ChaoFlash, SceneObject);

public:
    static void RegisterObject(Context* context);
    ChaoFlash(Context* context);

    void Start() override;
    void Update(float timeStep) override;
    void Set(const Vector3 position, int colorSet);
    void Disable() override;

private:
    float age_;
    RigidBody* rigidBody_;
    StaticModel* chaoModel_;
    Material* chaoMaterial_;
    Material* sunMaterial_;
};

#endif // CHAOFLASH_H
