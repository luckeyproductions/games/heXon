/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "enemy/baphomech.h"
#include "enemy/mason.h"
#include "enemy/razor.h"
#include "enemy/spire.h"
#include "environment/arena.h"
#include "environment/tile.h"
#include "fx/bubble.h"
#include "fx/flash.h"
#include "fx/line.h"
#include "fx/phaser.h"
#include "fx/repelring.h"
#include "pickup/chaoball.h"
#include "pickup/coin.h"
#include "pickup/coinpump.h"
#include "player/player.h"
#include "player/ship.h"
#include "projectile/brick.h"
#include "projectile/bullet.h"
#include "projectile/chaomine.h"
#include "projectile/chaozap.h"
#include "projectile/depthcharge.h"
#include "projectile/seeker.h"

#include "spawnmaster.h"

SpawnMaster::SpawnMaster(Context* context): Object(context),
    spawning_{ false },
    razorInterval_{ 2.f },
    sinceRazorSpawn_{ 0.f },
    spireInterval_{ 23.f },
    sinceSpireSpawn_{ 0.f },
    masonInterval_{ 123.f },
    sinceMasonSpawn_{ 0.f },
    bubbleInterval_{ .23f },
    sinceBubbleSpawn_{ bubbleInterval_ },
    sinceLastChaoPickup_{ 0.f },
    chaoInterval_{ CHAOINTERVAL }
{
}

void SpawnMaster::Prespawn()
{
    const float effectGain{ AUDIO->GetMasterGain(SOUND_EFFECT) };
    AUDIO->SetMasterGain(SOUND_EFFECT, 0.f);
    for (int r{ 0 }; r <   23; ++r) { Create<Razor>    (false); }
    for (int s{ 0 }; s <    7; ++s) { Create<Spire>    (false); }
    for (int m{ 0 }; m <    2; ++m) { Create<Mason>    (false); }
    for (int m{ 0 }; m <    8; ++m) { Create<ChaoMine> (false); }
    for (int s{ 0 }; s <   13; ++s) { Create<Seeker>   (false); }
    for (int s{ 0 }; s <   13; ++s) { Create<Brick>    (false); }
    for (int h{ 0 }; h <   16; ++h) { Create<HitFX>    (false); }
    for (int e{ 0 }; e <    9; ++e) { Create<Explosion>(false); }
    for (int f{ 0 }; f <   13; ++f) { Create<Flash>    (false); }
    for (int b{ 0 }; b <   42; ++b) { Create<Bubble>   (false); }
    for (int l{ 0 }; l < 2048; ++l) { Create<Line>     (false); }
    for (int z{ 0 }; z <    8; ++z) { Create<ChaoZap>  (false); }
    AUDIO->SetMasterGain(SOUND_EFFECT, effectGain);
}

void SpawnMaster::Activate()
{
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(SpawnMaster, HandleUpdate));
}
void SpawnMaster::Deactivate()
{
    UnsubscribeFromAllEvents();
}
void SpawnMaster::Clear()
{
    for (SharedPtr<Node> n: MC->scene_->GetChildren())
    {
        for (SharedPtr<Component> c: n->GetComponents())
        {
            if (c->IsInstanceOf<Enemy>()
             || c->IsInstanceOf<Effect>()
             || c->IsInstanceOf<Seeker>()
             || c->IsInstanceOf<Brick>()
             || c->IsInstanceOf<Coin>()
             || c->IsInstanceOf<Bullet>()
             || c->IsInstanceOf<RepelRing>()
             || c->IsInstanceOf<DepthCharge>())
            {
                SceneObject* s{ static_cast<SceneObject*>(c.Get()) };
                s->Disable();
                break;
            }
        }
    }

    if (MC->chaoBall_)
        MC->chaoBall_->Disable();
}

void SpawnMaster::Restart()
{
    Clear();
    razorInterval_   =   2.f;
    sinceRazorSpawn_ =   0.f;
    spireInterval_   =  23.f;
    sinceSpireSpawn_ =   0.f;
    masonInterval_   = 123.f;
    sinceMasonSpawn_ =   0.f;

    sinceLastChaoPickup_ = 0.f;
    chaoInterval_ = CHAOINTERVAL;

    Activate();
}

void SpawnMaster::SpawnDeathFlower(Vector3 position)
{
    unsigned radius{ static_cast<unsigned>(2 + 2 * Random(3)) };
    unsigned petals{ static_cast<unsigned>(Random(7)) };
    const float farOut{ ARENA_RADIUS - radius * 2.f };

    float typeFactor{ MC->SinceLastReset() };
    for (Ship* s: Ship::ships_)
    {
        if (s->IsEnabled())
            typeFactor -= Max(0.f, 10.f - s->GetHealth());
    }

    const bool masonic{ typeFactor > 100.f };

    if (position.Length() > farOut)
        position = position.Normalized() * farOut;

    const Vector3 spawnPosition{ NearestGridPoint(position) + Vector3::DOWN * 13.f };

    if (masonic)
    {
        radius = Max(4u, radius);
        Create<Mason>()->Set(spawnPosition);
    }
    else
    {
        Create<Spire>()->Set(spawnPosition);
    }

    if      (petals < 2) petals = 2;
    else if (petals < 5) petals = 3;
    else                 petals = 6;

    const Vector3 initialOffset{ Quaternion{ 60.f * Random(3), Vector3::UP } * Vector3::RIGHT };
    for (unsigned p{ 0u }; p < petals; ++p)
    {
        Vector3 petalPos{ spawnPosition + Vector3::DOWN * 10.f * (p + 1) +
                          Quaternion{ 60.f * p * (6 / petals), Vector3::UP } * initialOffset * radius };

        if (masonic)
            Create<Spire>()->Set(petalPos);
        else
            Create<Razor>()->Set(petalPos);
    }
}

Vector3 SpawnMaster::NearestGridPoint(const Vector3& position)
{

    const float tileWidth{ 2.f };
    const float rowSpacing{ 1.8f };
    const Vector2 scalar{ .5f * tileWidth, rowSpacing };

    Vector2 flatPos{ position.x_, position.z_ };
    IntVector2 unified{ VectorFloorToInt(flatPos / scalar) };
    const bool flip{ (Abs(unified.x_) % 2) == (Abs(unified.y_) % 2)};

    const Vector2 normal{ scalar.Normalized() };
    Vector2 local{ flatPos - scalar * Vector2(unified) };

    if (flip)
        local.x_ = scalar.x_ - local.x_;

    if (local.DotProduct(normal) > (.5f * scalar.Length()))
    {
        ++unified.y_;
        if (!flip)
            ++unified.x_;
    }
    else if (flip)
    {
        ++unified.x_;
    }

    flatPos = scalar * Vector2(unified);

    return Vector3{ flatPos.x_, 0.f, flatPos.y_ + .5f * rowSpacing };
}

void SpawnMaster::SpawnPattern()
{
    SpawnDeathFlower(Vector3::ZERO);
}

Vector3 SpawnMaster::SpawnPoint(int fromEdge)
{
    fromEdge *= 2;
    const Vector3 originOffset{ Vector3::UP * 5.f };
    auto origin { [fromEdge, originOffset](){ return RandomGridPoint(fromEdge) + originOffset; } };

    PhysicsRaycastResult hitResult{};
    Ray tileRay{ origin(), Vector3::DOWN };

    int attempts{ 5 };
    bool staticObject{ false };

    while (MC->PhysicsRayCastSingle(hitResult, tileRay, 34.f) && (attempts > 0 || staticObject))
    {
        staticObject = hitResult.body_->GetLinearFactor().Length() < 1.e-9;
        --attempts;

        tileRay.origin_ = origin();
    }

    return Vector3::DOWN * 23.f + tileRay.origin_;
}

Vector3 SpawnMaster::RandomGridPoint(int fromEdge)
{
    return NearestGridPoint(LucKey::PolarPoint(Random(ARENA_RADIUS - fromEdge), Random(360.f)));
}

void SpawnMaster::HandleUpdate(StringHash /*eventType*/, VariantMap &eventData)
{
    if (!MC->scene_->IsUpdateEnabled())
        return;

    bool spawnBaphomech{ true };
    for (Player* p: MC->GetPlayers())
    {
        Ship* ship{ p->GetShip() };
        if (p->IsAlive())
            spawnBaphomech &= ship->GetWeaponLevel() == 23;

        if (!spawnBaphomech)
            break;
    }

    if (spawnBaphomech)
    {
        Create<Baphomech>()->Set(Vector3::DOWN * 23.f);
        MC->chaoBall_->Disable();

        UnsubscribeFromEvent(E_UPDATE);
        return;
    }

    const float timeStep{ eventData[Update::P_TIMESTEP].GetFloat() };

    sinceRazorSpawn_ += timeStep;
    sinceSpireSpawn_ += timeStep;
    sinceMasonSpawn_ += timeStep;

    if ((sinceRazorSpawn_ > razorInterval_ || CountActive<Razor>() == 0) && CountActive<Razor>() < MaxRazors())
    {
        Razor* razor{ Create<Razor>() };
        razor->Set(SpawnPoint());

        sinceRazorSpawn_ = 0.f;
        razorInterval_ = (7.f - SPAWN->CountActive<Ship>())
                * pow(.95f, ((MC->SinceLastReset()) + 10.f) / 10.f);
    }

    if (sinceSpireSpawn_ > spireInterval_ && CountActive<Spire>() < MaxSpires())
    {
        Spire* spire{ Create<Spire>() };
        spire->Set(SpawnPoint(2));

        sinceSpireSpawn_ = 0.f;
        spireInterval_ = (23.f - SPAWN->CountActive<Ship>() * 2)
                * pow(.95f, ((MC->scene_->GetElapsedTime() - MC->world.lastReset) + 42.f) / 42.f);
    }

    if (sinceMasonSpawn_ > masonInterval_ && CountActive<Mason>() < MaxMasons())
    {
        Mason* mason{ Create<Mason>() };
        mason->Set(SpawnPoint(3));

        sinceMasonSpawn_ = 0.f;
        masonInterval_ = (123.f - SPAWN->CountActive<Ship>() * 3)
                * pow(.95f, ((MC->scene_->GetElapsedTime() - MC->world.lastReset) + 123.f) / 123.f);

    }

    if (!MC->chaoBall_->IsEnabled() && MC->GetGameState() == GS_PLAY)
    {
        if (sinceLastChaoPickup_ > chaoInterval_)
            MC->chaoBall_->Respawn();
        else sinceLastChaoPickup_ += timeStep;
    }


    sinceBubbleSpawn_ += timeStep;

    if (sinceBubbleSpawn_ > bubbleInterval_)
    {
        Create<Bubble>()->Set(BubbleSpawnPoint());
        sinceBubbleSpawn_ = 0.f;
    }
}

Vector3 SpawnMaster::BubbleSpawnPoint()
{
    return Quaternion(( Random(5) - 2 ) * 60.f, Vector3::UP) *
            (Vector3::FORWARD * 20.f + Vector3::RIGHT * Random(-10.f, 10.f))
            + Vector3::DOWN * 23.f;
}

int SpawnMaster::MaxRazors()
{
    return Clamp(static_cast<int>(23 * MC->SinceLastReset() * .0042f), SPAWN->CountActive<Ship>() * 2, 23);
}

int SpawnMaster::MaxSpires()
{
    return Clamp(static_cast<int>(7 * MC->SinceLastReset() * .0023f), SPAWN->CountActive<Ship>(), 7);
}

int SpawnMaster::MaxMasons()
{
    return Clamp(static_cast<int>(3 * MC->SinceLastReset() * .0013f), 1 + SPAWN->CountActive<Ship>() / 2, 3);
}
