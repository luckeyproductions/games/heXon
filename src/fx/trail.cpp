#include "trail.h"


void Trail::RegisterObject(Context* context)
{
    context->RegisterFactory<Trail>();

    DRY_COPY_BASE_ATTRIBUTES(Drawable);
    DRY_MIXED_ACCESSOR_ATTRIBUTE("Material", GetMaterialAttr, SetMaterialAttr, ResourceRef, ResourceRef(Material::GetTypeStatic()), AM_DEFAULT);
    DRY_ACCESSOR_ATTRIBUTE("Segments", GetNumTails, SetNumTails, unsigned int, 10, AM_DEFAULT);
    DRY_ACCESSOR_ATTRIBUTE("Length", GetTailLength, SetTailLength, float, .25f, AM_DEFAULT);
    DRY_ACCESSOR_ATTRIBUTE("Width", GetWidthScale, SetWidthScale, float, 1.f, AM_DEFAULT);
    DRY_ACCESSOR_ATTRIBUTE("Start Color", GetColorForHead, SetColorForHead, Color, Color::WHITE, AM_DEFAULT);
    DRY_ACCESSOR_ATTRIBUTE("End Color", GetColorForTip, SetColorForTip, Color, Color::WHITE, AM_DEFAULT);
    DRY_ACCESSOR_ATTRIBUTE("Draw Vertical", GetDrawVertical, SetDrawVertical, bool, true, AM_DEFAULT);
    DRY_ACCESSOR_ATTRIBUTE("Draw Horizontal", GetDrawHorizontal, SetDrawHorizontal, bool, true, AM_DEFAULT);
    DRY_ACCESSOR_ATTRIBUTE("Match Node Rotation", GetMatchNodeOrientation, SetMatchNodeOrientation, bool, false, AM_DEFAULT);
}

Trail::Trail(Context* context): Drawable(context, DRAWABLE_GEOMETRY),
    j_{ 0 },
    geometry_{ SharedPtr<Geometry>(new Geometry(context)) },
    vertexBuffer_{ SharedPtr<VertexBuffer>(new VertexBuffer(context)) },
    indexBuffer_{ SharedPtr<IndexBuffer>(new IndexBuffer(context)) },
    transforms_{},
    bufferSizeDirty_{ false },
    bufferDirty_{ false },
    forceUpdateVertexBuffer_{ false },
    vertical_{ true },
    mirrored_{ false },
    matchNode_{ false },
    horizontal_{ true },
    tailLength_{ .25f },
    scale_{ 1.f },
    tailNum_{ 10 },
    previousPosition_{ Vector3::ZERO },
    tailMesh_{},
    activeTails_{},
    bbmin_{ Vector3::ZERO },
    bbmax_{ Vector3::ZERO },
    tailTipColor_{ Color::WHITE },
    tailHeadColor_{ Color::WHITE }
{
    geometry_->SetVertexBuffer(0, vertexBuffer_);
    geometry_->SetIndexBuffer(indexBuffer_);

    indexBuffer_->SetShadowed(false);

    transforms_[0] = Matrix3x4::IDENTITY;
    transforms_[1] = Matrix3x4(Vector3::ZERO, Quaternion(0, 0, 0), Vector3::ONE);

    batches_.Resize(1);
    batches_[0].geometry_ = geometry_;
    batches_[0].geometryType_ = GEOM_STATIC;
    batches_[0].worldTransform_ = &transforms_[0];
    batches_[0].numWorldTransforms_ = 2;

    // for debug
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    SetMaterial(cache->GetResource<Material>("Materials/TailGenerator.xml"));
}

Trail::~Trail()
{
}

void Trail::ProcessRayQuery(const RayOctreeQuery& query, PODVector<RayQueryResult>& results)
{
    // If no billboard-level testing, use the Drawable test
    return;
}

void Trail::Update(const FrameInfo& frame)
{
    Drawable::Update(frame);
}

void Trail::ClearPointPath()
{
    fullPointPath_.Clear();
}

void Trail::UpdateTail()
{
    const Vector3 worldPosition{ node_->GetWorldPosition() };
    const float path{ (previousPosition_ - worldPosition).Length() };

    if (path > tailLength_)
    {
        const Vector3 forwardMotion{ matchNode_ ? GetNode()->GetWorldDirection()
                                                : (previousPosition_ - worldPosition).Normalized() };
        const Tail newPoint{ worldPosition,
                             forwardMotion,
                             matchNode_ ? GetNode()->GetWorldRight()
                                        : forwardMotion.CrossProduct(Vector3::UP).Normalized() };

        //forceBuildMeshInWorkerThread_ = true;
        forceUpdateVertexBuffer_ = true;
        previousPosition_ = worldPosition;
        fullPointPath_.Push(newPoint);

        if (fullPointPath_.Size() > tailNum_)
            fullPointPath_.Erase(0, fullPointPath_.Size() - tailNum_);
    }
}

void  Trail::DrawDebugGeometry(DebugRenderer *debug, bool depthTest)
{
    Drawable::DrawDebugGeometry(debug, depthTest);

    debug->AddNode(node_);

    for (unsigned i{ 0 }; i < fullPointPath_.Size() - 1; i++)
        debug->AddLine(fullPointPath_[i].position_, fullPointPath_[i + 1].position_, Color::RED.ToUInt(), false);
}

void Trail::UpdateBatches(const FrameInfo& frame)
{
    // Update tail's mesh if needed
    UpdateTail();

    // Update information for renderer about this drawable
    distance_ = frame.camera_->GetDistance(GetWorldBoundingBox().Center());
    batches_[0].distance_ = distance_;

    //batches_[0].numWorldTransforms_ = 2;

    // TailGenerator positioning
    //transforms_[0] = Matrix3x4::IDENTITY;
    // TailGenerator rotation
    //transforms_[1] = Matrix3x4(Vector3::ZERO, Quaternion(0, 0, 0), Vector3::ONE);
}

void Trail::UpdateGeometry(const FrameInfo& frame)
{
    if (bufferSizeDirty_ || indexBuffer_->IsDataLost())
        UpdateBufferSize();

    if (bufferDirty_ || vertexBuffer_->IsDataLost() || forceUpdateVertexBuffer_)
        UpdateVertexBuffer(frame);
}

UpdateGeometryType Trail::GetUpdateGeometryType()
{
    if (bufferDirty_ || bufferSizeDirty_ || vertexBuffer_->IsDataLost() || indexBuffer_->IsDataLost()|| forceUpdateVertexBuffer_)
        return UPDATE_MAIN_THREAD;
    else
        return UPDATE_NONE;
}

void Trail::SetMaterial(Material* material)
{
    batches_[0].material_ = material;
    MarkNetworkUpdate();
}

void Trail::OnWorldBoundingBoxUpdate()
{
    //worldBoundingBox_.Define(-M_LARGE_VALUE, M_LARGE_VALUE);
    worldBoundingBox_.Merge(bbmin_);
    worldBoundingBox_.Merge(bbmax_);
    worldBoundingBox_.Merge(node_->GetWorldPosition());
}

void Trail::UpdateBufferSize()
{
    unsigned numTails = tailNum_;

    if (!numTails)
        return;

    const int vertsPerSegment{ 2 * (vertical_ + horizontal_) };
    int degenerateVertCt{ 0 };

    if (vertsPerSegment > 2)
        degenerateVertCt += 2; //requires two degenerate triangles

    if (vertexBuffer_->GetVertexCount() != (numTails * vertsPerSegment))
        vertexBuffer_->SetSize((numTails * vertsPerSegment), MASK_POSITION | MASK_COLOR | MASK_TEXCOORD1, true);

    if (indexBuffer_->GetIndexCount() != (numTails * vertsPerSegment) + degenerateVertCt)
        indexBuffer_->SetSize((numTails * vertsPerSegment) + degenerateVertCt, false);

    bufferSizeDirty_ = false;
    bufferDirty_ = true;

    // Indices do not change for a given tail generator capacity
    unsigned short* dest{ (unsigned short*)indexBuffer_->Lock(0, (numTails * vertsPerSegment) + degenerateVertCt, true) };
    if (!dest)
        return;

    unsigned vertexIndex{ 0 };

    if (horizontal_)
    {
        unsigned stripsLen{ numTails };

        while (stripsLen--)
        {

            dest[0] = vertexIndex;
            dest[1] = vertexIndex + 1;
            dest += 2;

            // degenerate triangle vert on horizontal
            if (vertical_ && stripsLen == 0)
            {
                dest[0] = vertexIndex + 1;
                dest += 1;
            }
            vertexIndex += 2;
        }
    }

    if (vertical_)
    {
        unsigned stripsLen{ numTails };

        while (stripsLen--)
        {
            // degenerate triangle vert on vertical
            if (horizontal_ && stripsLen == (numTails - 1))
            {
                dest[0] = vertexIndex;
                dest += 1;
            }

            dest[0] = vertexIndex;
            dest[1] = vertexIndex + 1;
            dest += 2;
            vertexIndex += 2;
        }
    }

    indexBuffer_->Unlock();
    indexBuffer_->ClearDataLost();
}

void Trail::UpdateVertexBuffer(const FrameInfo& /*frame*/)
{
    const unsigned fullPointPathSize{ fullPointPath_.Size() };
    const unsigned currentVisiblePathSize{ tailNum_ };

    // Clear previous mesh data
    tailMesh_.Clear();

    // build tail

    // if tail path is short and nothing to draw, exit
    if (fullPointPathSize < 2)
        return;

    activeTails_.Clear();

    const unsigned min_i{ fullPointPathSize < currentVisiblePathSize ? 0 : fullPointPathSize - currentVisiblePathSize };
    // Step 1 : collect actual point's info for build tail path
    for (unsigned i{ min_i }; i < fullPointPathSize - 1; ++i)
    {
        activeTails_.Push(fullPointPath_[i]);

        const Vector3&p{ fullPointPath_[i].position_ };

        // Math BoundingBox based on actual point
        if (p.x_ < bbmin_.x_) bbmin_.x_ = p.x_;
        if (p.y_ < bbmin_.y_) bbmin_.y_ = p.y_;
        if (p.z_ < bbmin_.z_) bbmin_.z_ = p.z_;

        if (p.x_ > bbmax_.x_) bbmax_.x_ = p.x_;
        if (p.y_ > bbmax_.y_) bbmax_.y_ = p.y_;
        if (p.z_ > bbmax_.z_) bbmax_.z_ = p.z_;
    }

    if (activeTails_.Size() < 2)
        return;

    Vector<Tail>& t = activeTails_;

    // generate strips of tris
    TailVertex v{};
    const float mixFactor{ 1.f / activeTails_.Size() };

    // Forward part of tail (strip in xz plane)
    if (horizontal_)
    {
        for (unsigned i{ 0 }; i < activeTails_.Size() || i < tailNum_; ++i)
        {
            --j_;

            const unsigned sub{ i < activeTails_.Size() ? i : activeTails_.Size() - 1 };
            const Color c{ tailTipColor_.Lerp(tailHeadColor_, mixFactor * i) };

            v.color_ = c.ToUInt();
            v.uv_ = Vector2{ 1.f, 0.f };
            v.position_ = t[sub].position_ + M_1_SQRT2 * Vector3::UP * scale_ * (j_ == 0);
            tailMesh_.Push(v);

            //v.color_ = c.ToUInt();
            v.uv_ = Vector2{ 0.f, 1.f };
            v.position_ = t[sub].position_ - t[sub].worldRight_ * M_1_SQRT2 * j_ * scale_;
            tailMesh_.Push(v);

            j_ += j_ == -1 ? 3 : 0;
        }

        if (mirrored_) //Used by Seekers
        {
            for (TailVertex& tv: tailMesh_)
                tv.position_ -= 2.f * (tv.position_ - node_->GetWorldPosition()).DotProduct(node_->GetWorldDirection()) * node_->GetWorldDirection();
        }

    }

    // Upper part of tail (strip in xy-plane)
    if (vertical_)
    {
        for (unsigned i{ 0 }; i < activeTails_.Size() || i < tailNum_; ++i)
        {
            const unsigned sub{ i < activeTails_.Size() ? i : activeTails_.Size() - 1 };
            const Color c{ tailTipColor_.Lerp(tailHeadColor_, mixFactor * i) };

            v.color_ = c.ToUInt();
            v.uv_ = Vector2{ 1.f, 0.f };
            const Vector3 up{ t[sub].forward_.CrossProduct(t[sub].worldRight_).Normalized() };
            v.position_ = t[sub].position_ + up * scale_;
            tailMesh_.Push(v);

            //v.color_ = c.ToUInt();
            v.uv_ = Vector2{ 0.f, 1.f };
            v.position_ = t[sub].position_ - up * scale_;
            tailMesh_.Push(v);
        }
    }


    // copy new mesh to vertex buffer
    const unsigned meshVertexCount{ tailMesh_.Size() };
    batches_[0].geometry_->SetDrawRange(TRIANGLE_STRIP, 0, meshVertexCount + (horizontal_ && vertical_ ? 2 : 0), false);
    // get pointer
    TailVertex* dest{ (TailVertex*)vertexBuffer_->Lock(0, meshVertexCount, true) };
    if (!dest)
        return;
    // copy to vertex buffer
    memcpy(dest, &tailMesh_[0], tailMesh_.Size() * sizeof(TailVertex));

    vertexBuffer_->Unlock();
    vertexBuffer_->ClearDataLost();

    // unmark flags
    bufferDirty_ = false;
    forceUpdateVertexBuffer_ = false;
}


void Trail::SetTailLength(float length)
{
    tailLength_ = length;
}

void Trail::SetColorForTip(const Color& c)
{
    tailTipColor_ = c;
}

void Trail::SetColorForHead(const Color& c)
{
    tailHeadColor_ = c;
}

void Trail::SetNumTails(unsigned num)
{
    // Prevent negative value being assigned from the editor
    if (num > M_MAX_INT)
        num = 0;

    if (num > MAX_TAILS)
        num = MAX_TAILS;

    bufferSizeDirty_ = true;
    tailNum_ = num;
}

unsigned Trail::GetNumTails()
{
    return tailNum_;
}

void Trail::SetDrawVertical(bool value)
{
    vertical_ = value;
    //SetupBatches();
}

void Trail::SetDrawHorizontal(bool value)
{
    horizontal_ = value;
    //SetupBatches();
}
void Trail::SetDrawMirrored(bool value)
{
    mirrored_ = value;
    //SetupBatches();
}

void Trail::SetMatchNodeOrientation(bool value)
{
    matchNode_ = value;
}

void Trail::MarkPositionsDirty()
{
    Drawable::OnMarkedDirty(node_);
    bufferDirty_ = true;
}

void Trail::SetMaterialAttr(const ResourceRef& value)
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    SetMaterial(cache->GetResource<Material>(value.name_));
}

ResourceRef Trail::GetMaterialAttr() const
{
    return GetResourceRef(batches_[0].material_, Material::GetTypeStatic());
}

void Trail::SetWidthScale(float scale)
{
    scale_ = scale;
}
