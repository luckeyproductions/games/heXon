/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BORDERLINE_H
#define BORDERLINE_H


#include "../luckey.h"

class BorderLine: public Drawable
{
    DRY_OBJECT(BorderLine, Drawable);

public:
    BorderLine(Context* context, unsigned char drawableFlags);
    static void RegisterObject(Context* context);

    void OnSetEnabled() override;
    void UpdateBatches(const FrameInfo& frame) override;
    void UpdateGeometry(const FrameInfo& frame) override;

    UpdateGeometryType GetUpdateGeometryType() override;
    bool DrawOcclusion(OcclusionBuffer* buffer) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnWorldBoundingBoxUpdate() override;
    void OnRemoveFromOctree() override;
};

#endif // BORDERLINE_H
