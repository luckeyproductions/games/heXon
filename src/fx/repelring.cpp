/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"

#include "repelring.h"

RepelRing::RepelRing(Context* context): SceneObject(context),
    ring_{ nullptr },
    age_{ 0.f }
{
    blink_ = false;
    SetUpdateEventMask(USE_UPDATE);
}

void RepelRing::Start()
{
    ring_ = node_->CreateComponent<StaticModel>();
    ring_->SetModel(RES(Model, "Models/Plane.mdl"));
}

void RepelRing::Set(const Vector3& position, int colorSet)
{
    SceneObject::Set(position, { Random(360.f), Vector3::UP });

    ring_->SetMaterial(MC->colorSets_[colorSet].repelMaterial_->Clone());

    age_ = 0.f;
}

void RepelRing::Update(float timeStep)
{
    age_ += timeStep;

    node_->SetScale(1.7f + 23.f * Cbrt(age_));
    Material* mat{ ring_->GetMaterial(0u) };
    Color col{ mat->GetShaderParameter("MatDiffColor").GetColor() };
    const float alpha{ 1.7f * (7.f * age_ - PowN(5.f * age_, 2)) };
    mat->SetShaderParameter("MatDiffColor", col.Transparent(alpha));

    if (age_ > 0.f && alpha <= 0.f)
        Disable();
}
