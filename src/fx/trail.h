#pragma once
#include <Dry/Core/Context.h>
#include <Dry/Scene/Node.h>
#include <Dry/Resource/ResourceCache.h>
#include <Dry/Graphics/OctreeQuery.h>
#include <Dry/Graphics/VertexBuffer.h>
#include <Dry/Graphics/IndexBuffer.h>
#include <Dry/Graphics/Geometry.h>
#include <Dry/Graphics/Drawable.h>
#include <Dry/Graphics/DebugRenderer.h>
#include <Dry/Graphics/Camera.h>
#include <Dry/Graphics/Material.h>

using namespace Dry;

/// Vertex struct for tail
struct DRY_API TailVertex
{
    Vector3 position_;
    unsigned color_;
    Vector2  uv_;
};

/// One billboard in the billboard set.
struct DRY_API Tail
{
    /// Position.
    Vector3 position_;
    Vector3 forward_;
    Vector3 worldRight_;
};

static const unsigned MAX_TAILS{ 65536 / 6 };

/// Custom component that creates a tail
class DRY_API Trail: public Drawable
{
    DRY_OBJECT(Trail, Drawable);

public:
    /// Construct.
    Trail(Context* context);
    /// Destruct.
    virtual ~Trail();
    /// Register object factory.
    static void RegisterObject(Context* context);
    /// Process octree raycast. May be called from a worker thread.
    void ProcessRayQuery(const RayOctreeQuery& query, PODVector<RayQueryResult>& results) override;
    ///
    void Update(const FrameInfo &frame) override;
    /// Calculate distance and prepare batches for rendering. May be called from worker thread(s), possibly re-entrantly.
    void UpdateBatches(const FrameInfo& frame) override;
    /// Prepare geometry for rendering. Called from a worker thread if possible (no GPU update.)
    void UpdateGeometry(const FrameInfo& frame) override;
    /// Return whether a geometry update is necessary, and if it can happen in a worker thread.
    UpdateGeometryType GetUpdateGeometryType() override;

    void  DrawDebugGeometry(DebugRenderer *debug, bool depthTest) override;

    /// Set material.
    void SetMaterial(Material* material);
    /// Set tail segment length
    void SetTailLength(float length);
    /// Get tail segment length
    float GetTailLength();
    /// Set count segments of all tail
    void SetNumTails(unsigned num);
    /// Get count segments of all tail
    unsigned GetNumTails();
    /// Set width scale of the tail
    void SetWidthScale(float scale);
    /// Set vertex blended color for tip of all tail. The alpha-value of new color resets by default to zero.
    void SetColorForTip(const Color& c);
    /// Set vertex blended color for head of all tail. The alpha-value of new color resets by default to one.
    void SetColorForHead(const Color& c);
    /// Set material attribute.
    void SetMaterialAttr(const ResourceRef& value);
    /// Return material attribute.
    ResourceRef GetMaterialAttr() const;

    /// Get whether to draw the vertical strip or not
    bool GetDrawVertical() const { return vertical_; }
    /// Get whether to draw the horizontal strip or not
    bool GetDrawHorizontal() const { return horizontal_; }
    /// Get whether or not this tail is matching node direction vectors
    bool GetMatchNodeOrientation() const { return matchNode_; }
    /// Set whether to draw the vertical strip or not
    void SetDrawVertical(bool value);
    /// Set whether to draw the horizontal strip or not
    void SetDrawHorizontal(bool value);
    /// Set whether or not this tail is matching node direction vectors
    void SetMatchNodeOrientation(bool value);
    ///
    float GetWidthScale() const { return scale_; }
    unsigned  GetNumTails() const { return tailNum_; }
    float GetTailLength() const { return tailLength_;  }
    const Color& GetColorForHead() const { return tailHeadColor_; }
    const Color& GetColorForTip() const { return tailTipColor_;  }

    void ClearPointPath();
    void SetDrawMirrored(bool value);

protected:
    /// Recalculate the world-space bounding box.
    void OnWorldBoundingBoxUpdate() override;
    /// Mark vertex buffer to need an update.
    void MarkPositionsDirty();

    /// Tails.
    PODVector<Tail> fullPointPath_;

private:
    int j_;
    /// Resize TailGenerator vertex and index buffers.
    void UpdateBufferSize();
    /// Rewrite TailGenerator vertex buffer.
    void UpdateVertexBuffer(const FrameInfo& frame);
    /// Update/Rebuild tail mesh only if position changed (called by UpdateBatches())
    void UpdateTail();
    /// Geometry.
    SharedPtr<Geometry> geometry_;
    /// Vertex buffer.
    SharedPtr<VertexBuffer> vertexBuffer_;
    /// Index buffer.
    SharedPtr<IndexBuffer> indexBuffer_;
    /// Transform matrices for position and orientation.
    Matrix3x4 transforms_[2];
    /// Buffers need resize flag.
    bool bufferSizeDirty_;
    /// Vertex buffer needs rewrite flag.
    bool bufferDirty_;
    ///
    bool forceUpdateVertexBuffer_;
    ///
    bool vertical_;
    ///
    bool mirrored_;
    ///
    bool matchNode_;
    ///
    bool horizontal_;
    ///
    float tailLength_;
    ///
    float scale_;
    ///
    unsigned tailNum_;
    /// Previous position of tail
    Vector3 previousPosition_;
    ///
    Vector<TailVertex> tailMesh_;
    ///
    Vector<Tail> activeTails_;
    ///
    Vector3 bbmin_;
    ///
    Vector3 bbmax_;
    ///
    Color tailTipColor_;
    ///
    Color tailHeadColor_;
};
