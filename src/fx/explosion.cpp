/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../pickup/coin.h"
#include "../pickup/coinpump.h"
#include "../projectile/chaomine.h"
#include "effectinstance.h"

#include "explosion.h"

void Explosion::RegisterObject(Context* context)
{
    context->RegisterFactory<Explosion>();
    context->MC->GetSample("Explode");
}

Explosion::Explosion(Context* context): Effect(context),
    playerID_{ 0 },
    initialMass_{ 3.f },
    initialBrightness_{ 8.f },
    fixedAge_{},
    blastForce_{}
{
    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void Explosion::Start()
{
    Effect::Start();

    MC->arena_->AddToAffectors(node_);

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(initialMass_);
    rigidBody_->SetLinearFactor(Vector3::ONE - Vector3::UP);

    light_ = node_->CreateComponent<Light>();
    light_->SetRange(10.0f);
    light_->SetBrightness(initialBrightness_);

    particleEmitter_->SetEffect(RES(ParticleEffect, "Particles/Explosion.xml"));

    sampleSource_ = node_->CreateComponent<SoundSource>();
    sampleSource_->SetSoundType(SOUND_EFFECT);
}

void Explosion::Update(float timeStep)
{
    Effect::Update(timeStep);

    if (light_->IsEnabled())
        light_->SetBrightness(Max(initialBrightness_ * (.32f - age_) / .32f, 0.f));
}

void Explosion::FixedUpdate(float timeStep)
{
    rigidBody_->SetMass(Max(initialMass_ * ((.1f - fixedAge_) / .1f), .01f));
    fixedAge_ += timeStep;

    if (rigidBody_->GetMass() < .023f)
        return;

    if (node_->IsEnabled() && MC->scene_->IsUpdateEnabled())
    {

        PODVector<RigidBody*> hitResults{};
        float radius{ 2.f * initialMass_ + fixedAge_ * 7.f };

        if (MC->PhysicsSphereCast(hitResults, node_->GetPosition(), radius, M_MAX_UNSIGNED))
        {
            for (RigidBody* h : hitResults)
            {
                Node* hitNode{ h->GetNode() };
                if (hitNode->GetName() == "PickupTrigger")
                {
                    hitNode = hitNode->GetParent();
                    h = hitNode->GetComponent<RigidBody>();
                }

                const Vector3 hitNodeWorldPos{ hitNode->GetWorldPosition() };
                if ((!h->IsTrigger() || hitNode->HasComponent<Coin>()) && h->GetPosition().y_ > -.1f)
                {
                    //positionDelta is used for force calculation
                    const Vector3 positionDelta{ hitNodeWorldPos - node_->GetWorldPosition() };
                    const float distance{ positionDelta.Length() };
                    const Vector3 force{ positionDelta.Normalized() * Max(radius - distance, 0.f)
                                * blastForce_ * (rigidBody_->GetMass() - .01f) };
                    h->ApplyForce(force * timeStep);

                    //Deal damage
                    float damage{ rigidBody_->GetMass() * timeStep };

                    Enemy* e{ h->GetNode()->GetDerivedComponent<Enemy>() };

                    if (e && !e->IsInstanceOf<ChaoMine>() && !e->IsInstanceOf<CoinPump>())
                        e->Hit(damage, playerID_);
                }
            }
        }
    }
}

void Explosion::Set(const Vector3& position, const Color& color, const float size, int colorSet, bool bubbles)
{
    playerID_ = colorSet;
    Effect::Set(position);
    fixedAge_ = 0.f;
    blastForce_ = 2342.f;

    node_->SetScale(size);
    initialMass_ = Sqrt(2.f + size * 3.f) * 1.7f;
    rigidBody_->SetMass(initialMass_);

    if (light_->IsEnabled())
    {
        light_->SetColor(color);
        light_->SetBrightness(initialBrightness_);
    }

    if (bubbles)
    {
        EffectInstance* bubbles{ SPAWN->Create<EffectInstance>() };
        ParticleEffect* bubbleEffect{ RES(ParticleEffect, "Particles/ExplosionBubbles.xml") };
        bubbleEffect->SetMaxVelocity(size * 13.f);
        bubbles->Set(GetWorldPosition(), bubbleEffect);
    }

    ParticleEffect* particleEffect{ particleEmitter_->GetEffect() };
    Vector<ColorFrame> colorFrames{};
    colorFrames.Push(ColorFrame{ Color{ 0.f, 0.f, 0.f, 0.f }, 0.f });
    colorFrames.Push(ColorFrame{ color, .1f });
    colorFrames.Push(ColorFrame{ color * .1f, .35f });
    colorFrames.Push(ColorFrame{ Color{ 0.f, 0.f, 0.f, 0.f }, 0.f });
    particleEffect->SetColorFrames(colorFrames);

    PlaySample(MC->GetSample("Explode"),   Min(.5f  + .25f  * size, 1.f));
    PlaySample(MC->GetSample("Explode_s"), Min(.25f + .125f * size, .23f), false);
}
