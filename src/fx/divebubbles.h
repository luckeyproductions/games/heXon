/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DIVEBUBBLES_H
#define DIVEBUBBLES_H

#include "../mastercontrol.h"

struct Bubble
{
    float size_{ .1f + RandomOffCenter(.05f) };
    Vector3 position_{ Vector3::ZERO };
    Vector3 velocity_{ Vector3::UP };
    float age_{ 0.f };
    float lifeTime_{ Random(.55f, 1.7f) };

    bool operator !=(const Bubble& rhs) const
    {
        return position_ != rhs.position_;
    }
};

class DiveBubbles: public LogicComponent
{
    DRY_OBJECT(DiveBubbles, LogicComponent);

public:
    DiveBubbles(Context* context);
    void Start() override;
    void Stop() override;
    void Clear();
    void Update(float timeStep) override;

    void SetShip(Ship* ship) { ship_ = ship; }

private:
    void AddBubbles();

    Ship* ship_;
    BillboardSet* billboardSet_;
    PODVector<Bubble> bubbles_;
    float sinceBubble_;
};

#endif // DIVEBUBBLES_H
