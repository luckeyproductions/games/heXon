/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SPAWNMASTER_H
#define SPAWNMASTER_H

#include "mastercontrol.h"
#include "sceneobject.h"

#define CHAOINTERVAL Random(23.0f, 55.0f)

class SpawnMaster: public Object
{
    friend class MasterControl;
    DRY_OBJECT(SpawnMaster, Object);

public:
    SpawnMaster(Context* context);

    void Clear();
    Vector3 SpawnPoint(int fromEdge = 0);
    static Vector3 NearestGridPoint(const Vector3& position);
    static Vector3 RandomGridPoint(int fromEdge = 0);

    void ChaoPickup() { sinceLastChaoPickup_ = 0.f; chaoInterval_ = CHAOINTERVAL; }

    template <class T> T* Create(bool recycle = true)
    {
        T* created{ nullptr };

        if (recycle) {

            for (Node* n: created_[T::GetTypeStatic()]) {

                if (!n->IsEnabled()) {

                    created = n->GetComponent<T>();
                    break;
                }
            }
        }

        if (!created)
        {
            Node* spawnedNode{ MC->scene_->CreateChild(T::GetTypeStatic().ToString()) };
            created = spawnedNode->CreateComponent<T>();
            spawnedNode->SetEnabledRecursive(false);

            if (!created_.Contains(T::GetTypeStatic()))
                created_[T::GetTypeStatic()] = Vector<Node*>();

            created_[T::GetTypeStatic()].Push(spawnedNode);
        }

        return created;
    }

    Vector<Node*> GetAll() const
    {
        Vector<Node*> all{};
        for (const Vector<Node*>& allOfType: created_.Values())
            all += allOfType;
        return all;
    }
    template <class T> Vector<Node*> GetAll()
    {
        return created_[T::GetTypeStatic()];
    }
    template <class T> Vector<Node*> GetActive()
    {
        Vector<Node*> nodes{ GetAll<T>() };

        for (Node* n: nodes)
        {
            if (!n->IsEnabled())
                nodes.RemoveSwap(n);
        }

        return nodes;
    }
    template <class T> int CountActive()
    {
        return GetActive<T>().Size();
    }

    void Prespawn();
    
    void SpawnPattern();
    void SpawnDeathFlower(Vector3 position);

private:
    HashMap<StringHash, Vector<Node*>> created_;

    bool spawning_;

    float razorInterval_;
    float sinceRazorSpawn_;

    float spireInterval_;
    float sinceSpireSpawn_;

    float masonInterval_;
    float sinceMasonSpawn_;

    float bubbleInterval_;
    float sinceBubbleSpawn_;

    float sinceLastChaoPickup_;
    float chaoInterval_;

    void HandleUpdate(StringHash eventType, VariantMap &eventData);

    Vector3 BubbleSpawnPoint();
    int MaxRazors();
    int MaxSpires();
    int MaxMasons();

    void Activate();
    void Deactivate();
    void Restart();
};

#endif // SPAWNMASTER_H
