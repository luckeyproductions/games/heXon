/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <fstream>
#include "settings.h"
#include "effectmaster.h"
#include "spawnmaster.h"
#include "inputmaster.h"
#include "hexocam.h"
#include "enemy/baphomech.h"
#include "enemy/mason.h"
#include "enemy/razor.h"
#include "enemy/spire.h"
#include "environment/arena.h"
#include "environment/door.h"
#include "environment/highest.h"
#include "environment/lobby.h"
#include "environment/panel.h"
#include "environment/splatterpillar.h"
#include "environment/tile.h"
#include "fx/bubble.h"
#include "fx/effectinstance.h"
#include "fx/explosion.h"
#include "fx/flash.h"
#include "fx/hitfx.h"
#include "fx/line.h"
#include "fx/mirage.h"
#include "fx/muzzle.h"
#include "fx/phaser.h"
#include "fx/soundeffect.h"
#include "fx/trail.h"
#include "pickup/apple.h"
#include "pickup/chaoball.h"
#include "pickup/coin.h"
#include "pickup/coinpump.h"
#include "pickup/heart.h"
#include "player/gui3d.h"
#include "player/pilot.h"
#include "player/player.h"
#include "player/ship.h"
#include "projectile/brick.h"
#include "projectile/bullet.h"
#include "projectile/chaoflash.h"
#include "projectile/chaomine.h"
#include "projectile/chaozap.h"
#include "projectile/seeker.h"
#include "ui/menumaster.h"
#include "ui/settingspage.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context* context): Application(context),
    players_{},
    storagePath_{},
    paused_{ false },
    currentState_{ GS_INTRO },
    sinceStateChange_{ .0f },
    menuMusic_{ nullptr },
    gameMusic_{ nullptr },
    bossMusic_{ nullptr },
    secondsPerFrame_{ 1.f },
    sinceFrameRateReport_{ .0f }
{
}

void MasterControl::Setup()
{
    context_->RegisterSubsystem(this);
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = FILES->GetAppPreferencesDir("luckey", "logs") + "heXon.log";
    engineParameters_[EP_WINDOW_TITLE] = "heXon";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_VSYNC] = true;

    //Add resource paths
    String resourcePaths{ "Resources" };
    if (!FILES->DirExists(AddTrailingSlash(FILES->GetProgramDir()) + resourcePaths))
    {
        const String installedResources{ RemoveTrailingSlash(RESOURCEPATH) };
        if (FILES->DirExists(installedResources))
            resourcePaths = installedResources;
    }

    if (resourcePaths == "Resources")
        storagePath_ = resourcePaths;
    else
        storagePath_ = RemoveTrailingSlash(FILES->GetAppPreferencesDir("luckey", "hexon"));

    resourcePaths += ";";

    if (FILES->DirExists("Data"))
        resourcePaths += "Data;";
    if (FILES->DirExists("CoreData"))
        resourcePaths += "CoreData;";

    engineParameters_[EP_RESOURCE_PATHS] = resourcePaths;

//    engineParameters_[EP_REFRESH_RATE] = ;
//    engineParameters_[EP_HEADLESS] = true;
//    engineParameters_[EP_BORDERLESS] = true;

    Settings* settings{ new Settings{ context_ } };
    context_->RegisterSubsystem(settings);

    if (settings->Load())
    {
        engineParameters_[EP_WINDOW_WIDTH] = settings->GetResolution().x_;
        engineParameters_[EP_WINDOW_HEIGHT] = settings->GetResolution().y_;
        engineParameters_[EP_FULL_SCREEN] = settings->GetFullscreen();
        engineParameters_[EP_VSYNC] = settings->GetVSync();
    }
}

void MasterControl::Start()
{
    CreateColorSets();

    Trail::RegisterObject(context_);
    AnimatedBillboardSet::RegisterObject(context_);
    Mirage::RegisterObject(context_);

    heXoCam::RegisterObject(context_);
    Lobby::RegisterObject(context_);
    Door::RegisterObject(context_);
    SplatterPillar::RegisterObject(context_);
    Arena::RegisterObject(context_);
    Apple::RegisterObject(context_);
    Heart::RegisterObject(context_);
    ChaoBall::RegisterObject(context_);
    Tile::RegisterObject(context_);
    Highest::RegisterObject(context_);
    Ship::RegisterObject(context_);
    Pilot::RegisterObject(context_);
    Bullet::RegisterObject(context_);
    Muzzle::RegisterObject(context_);
    Phaser::RegisterObject(context_);
    GUI3D::RegisterObject(context_);
    Panel::RegisterObject(context_);

    ChaoFlash::RegisterObject(context_);
    ChaoMine::RegisterObject(context_);
    ChaoZap::RegisterObject(context_);
    Coin::RegisterObject(context_);
    CoinPump::RegisterObject(context_);

    Razor::RegisterObject(context_);
    Spire::RegisterObject(context_);
    Seeker::RegisterObject(context_);
    Mason::RegisterObject(context_);
    Brick::RegisterObject(context_);
    Baphomech::RegisterObject(context_);

    EffectInstance::RegisterObject(context_);
    SoundEffect::RegisterObject(context_);
    HitFX::RegisterObject(context_);
    Bubble::RegisterObject(context_);
    Flash::RegisterObject(context_);
    Explosion::RegisterObject(context_);
    Line::RegisterObject(context_);

    context_->RegisterFactory<SettingsPage>();

    context_->RegisterSubsystem(new EffectMaster(context_));
    context_->RegisterSubsystem(new InputMaster(context_));
    context_->RegisterSubsystem(new SpawnMaster(context_));

    Graphics* graphics{ GetSubsystem<Graphics>() };
    if (graphics)
    {
        Engine* engine{ GetSubsystem<Engine>() };
        engine->SetMaxFps(graphics->GetRefreshRate());

        // Precache shaders if possible
        if (!engine->IsHeadless() && CACHE->Exists("Shaders/Shaders.xml"))
            graphics->PrecacheShaders(*CACHE->GetFile("Shaders/Shaders.xml"));

        CreateUI();

//        GRAPHICS->BeginDumpShaders("Resources/Shaders/LatestShaders.xml");
    }

    CreateScene();
    SetGameState(GS_LOBBY);
    SubscribeToEvents();
}

void MasterControl::Stop()
{
    GetSubsystem<Settings>()->Save();

//    engine_->DumpResources(true);
//    GRAPHICS->EndDumpShaders();
}

void MasterControl::SubscribeToEvents()
{
    //Subscribe scene update event.
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(MasterControl, HandleUpdate));
    SubscribeToEvent(E_JOYSTICKCONNECTED, DRY_HANDLER(MasterControl, HandleJoystickConnected));
//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(MasterControl, HandlePostRenderUpdate));
}

void MasterControl::CreateUI()
{
    UI* ui{ GetSubsystem<UI>() };
    ui->SetScale(GRAPHICS->GetHeight() / 1080.f);

    Cursor* cursor{ ui->GetRoot()->CreateChild<Cursor>() };
    world.cursor.uiCursor = cursor;
    ui->SetCursor(cursor);
    cursor->DefineShape(CS_NORMAL, RES(Image, "UI/Cursor.png"), { 0, 0, 96, 96 }, { 2, 3 });
    cursor->SetBlendMode(BLEND_ALPHA);
    cursor->SetPosition(GRAPHICS->GetWidth() / 2, GRAPHICS->GetHeight() / 2);
    cursor->SetOpacity(0.f);

    Input* input{ GetSubsystem<Input>() };
    input->SetMouseMode(MM_RELATIVE);

    context_->RegisterSubsystem(new MenuMaster(context_));
}

Sound* MasterControl::GetMusic(const String& name) const
{
    Sound* song{ RES(Sound, "Music/" + name + ".ogg") };
    song->SetLooped(true);

    return song;
}

Sound* MasterControl::GetSample(const String& name)
{
//    unsigned nameHash{ name.ToHash() };

//    if (samples_.Contains(nameHash))
//        return samples_[nameHash].Get();

    Sound* sample{ RES(Sound, "Samples/" + name + ".wav") };
//    samples_[nameHash] = sample;
//    playedSamples_.Insert(nameHash);

    return sample;
}

void MasterControl::CreateColorSets()
{
    for (int c: { 0, 1, 2, 3, 4 })
    {
        ColorSet set{};

        switch (c)
        {
        case 0: set.colors_.first_  = Color{ .23f, .5f  , 1.0f };
                set.colors_.second_ = Color{ .05f, .05f , .05f };
            break;
        case 1: set.colors_.first_  = Color{ .38f, .42f , .01f };
                set.colors_.second_ = Color{ .1f , .3f  , .05f };
            break;
        case 2: set.colors_.first_  = Color{ .5f , .32f , .01f };
                set.colors_.second_ = Color{ .16f, .0f  , .38f };
            break;
        case 3: set.colors_.first_  = Color{ .45f, .1f  , .42f };
                set.colors_.second_ = Color{ .0f , .27f , .42f };
            break;
        case 4: set.colors_.first_  = Color{ .42f, .023f, .01f };
                set.colors_.second_ = Color{ .34f, .34f , .34f };
        default: break;
        }

        set.glowMaterial_   = RES(Material, "Materials/Glow.xml")->Clone();
        set.hullMaterial_   = RES(Material, "Materials/Hull.xml")->Clone();
        set.bulletMaterial_ = RES(Material, "Materials/Bullet.xml")->Clone();
        set.repelMaterial_  = RES(Material, "Materials/Repel.xml")->Clone();
        set.addMaterial_    = RES(Material, "Materials/Add.xml")->Clone();
        set.panelMaterial_  = RES(Material, "Materials/Panel.xml")->Clone();

        set.glowMaterial_->SetShaderParameter("MatEmissiveColor", set.colors_.first_);
        set.glowMaterial_->SetShaderParameter("MatDiffColor", set.colors_.first_ * .23f);
        set.glowMaterial_->SetShaderParameter("MatSpecColor", (set.colors_.first_ + Color::WHITE) * .05f);
        set.hullMaterial_->SetShaderParameter("MatDiffColor", set.colors_.second_);
        set.hullMaterial_->SetShaderParameter("MatSpecColor", set.colors_.second_.Lerp(Color::WHITE, .13f));
        set.bulletMaterial_->SetShaderParameter("MatDiffColor", set.colors_.first_);
        set.repelMaterial_->SetShaderParameter("MatDiffColor", set.colors_.first_);
        set.addMaterial_->SetShaderParameter("MatDiffColor", set.colors_.first_ * 1.13f);
        set.panelMaterial_->SetShaderParameter("MatEmissiveColor", set.colors_.first_ * .666f);
        set.panelMaterial_->SetShaderParameter("MatDiffColor", set.colors_.first_ * .23f);
        set.panelMaterial_->SetShaderParameter("MatSpecColor", (set.colors_.first_ + Color::WHITE) * .23f);

        SharedPtr<Material> flash{ RES(Material, "Materials/Flash.xml")->Clone() };
        flash->SetShaderParameter("MatDiffColor", set.colors_.first_ * 1.23f);
        set.hitFx_ = RES(ParticleEffect, "Particles/HitFX.xml")->Clone();
        set.hitFx_->SetMaterial(flash);

        colorSets_[c] = set;
    }
}

void MasterControl::HandleJoystickConnected(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (INPUT->GetNumJoysticks() > NumPlayers())
        AddPlayer();
}

void MasterControl::AddPlayer()
{
    if (NumPlayers() >= 4u)
        return;

    int playerId{ 1 };
    Vector<int> takenIds{};

    for (Player* player: players_)
        takenIds.Push(player->GetPlayerId());

    while (takenIds.Contains(playerId))
        ++playerId;

    Player* newPlayer{ new Player(playerId, context_) };
    players_.Push(SharedPtr<Player>(newPlayer));

    Pilot* pilot{ SPAWN->Create<Pilot>(GetGameState() == GS_LOBBY) };
    GetSubsystem<InputMaster>()->SetPlayerControl(playerId, pilot);
    pilot->Initialize();

    if (GetGameState() == GS_LOBBY)
        newPlayer->EnterLobby();
}

void MasterControl::RemoveAutoPilot()
{
    if (players_.Size() > 1 || NoHumans())
    {
        for (Pilot* pilot: GetComponentsInScene<Pilot>())
        {
            Player* player{ pilot->GetPlayer() };

            if (player && !player->IsHuman())
            {
                pilot->LeaveLobby();
                return;
            }
        }
    }
}

void MasterControl::RemovePlayer(Player* player)
{
    if (player->gui3d_)
        player->gui3d_->SetScore(0);

    Player::takenColorSets_.Erase(player->GetPlayerId());
    InputMaster* inputMaster{ GetSubsystem<InputMaster>() };
    Controllable* controllable{ inputMaster->GetControllableByPlayer(player->GetPlayerId()) };

    if (controllable)
    {
        controllable->ClearControl();
        inputMaster->SetPlayerControl(player->GetPlayerId(), nullptr);
    }

    players_.Remove(SharedPtr<Player>(player));
}

void MasterControl::CreateScene()
{
    scene_ = new Scene{ context_ };
    scene_->SetTimeScale(1.23f);

    world.octree = scene_->CreateComponent<Octree>();
    world.octree->SetSize({ -42.f * Vector3::ONE, 42.f * Vector3::ONE }, 8);
    physicsWorld_ = scene_->CreateComponent<PhysicsWorld>();
//    physicsWorld_->SetGravity(Vector3::ZERO);
//    physicsWorld_->SetMaxSubSteps(5);
    physicsWorld_->SetSplitImpulse(true);

    scene_->CreateComponent<DebugRenderer>();

    //Create a Zone component for fog control
    Node* zoneNode{ scene_->CreateChild("Zone") };
    Zone* zone{ zoneNode->CreateComponent<Zone>() };
    zone->SetBoundingBox({{ -100.f, -100.f, -100.f }, { 100.f, 5.f, 100.f } });
    zone->SetFogColor(Color::BLACK);
    zone->SetFogStart(60.f);
    zone->SetFogEnd(62.3f);
    zone->SetHeightFog(true);
    zone->SetFogHeight(-10.f);
    zone->SetFogHeightScale(.23f);
    zone->SetAmbientColor(Color::BLACK.Lerp(Color::WHITE, .55f));

    //Create cursor
    world.cursor.sceneCursor = scene_->CreateChild("Cursor");
    StaticModel* cursorObject{ world.cursor.sceneCursor->CreateComponent<StaticModel>() };
    cursorObject->SetModel(RES(Model, "Models/Hexagon.mdl"));
    cursorObject->SetMaterial(RES(Material, "Materials/Glow.xml"));
    world.cursor.sceneCursor->SetEnabled(false);

    //Create an solid black plane to hide everything beyond full fog
    world.voidNode = scene_->CreateChild("Void");
    world.voidNode->SetPosition(Vector3::DOWN * 10.f);
    world.voidNode->SetScale({ 1e3f, 1.f, 1e3f });
    StaticModel* planeObject{ world.voidNode->CreateComponent<StaticModel>() };
    planeObject->SetModel(RES(Model, "Models/Plane.mdl"));
    planeObject->SetMaterial(RES(Material, "Materials/PitchBlack.xml"));
    planeObject->SetOccluder(true);

    //Create camera
    if (GRAPHICS)
    {
        Node* cameraNode{ scene_->CreateChild("Camera", LOCAL) };
        world.camera = cameraNode->CreateComponent<heXoCam>();
    }

    //Create arena
    Node* arenaNode{ scene_->CreateChild("Arena", LOCAL) };
    arena_ = arenaNode->CreateComponent<Arena>();
    context_->RegisterSubsystem(arena_);

    SPAWN->Prespawn();

    //Construct lobby
    Node* lobbyNode{ scene_->CreateChild("Lobby", LOCAL) };
    lobby_ = lobbyNode->CreateComponent<Lobby>();
    //Create ships
    for (int s: { 3, 4, 2, 1 })
    {
        const Quaternion rotation{ 60.f + ((s % 2) * 60.f - (s / 2) * 180.f), Vector3::UP };
        const Vector3 position{ rotation * Vector3{ 0.f, .6f, 2.3f } };

        SPAWN->Create<Ship>()->Set(position, rotation);
    }

    apple_ = SPAWN->Create<Apple>();
    heart_ = SPAWN->Create<Heart>();
    chaoBall_ = SPAWN->Create<ChaoBall>();
    chaoBall_->Disable();

    NavigationMesh* navMesh{ scene_->CreateComponent<NavigationMesh>() };
    navMesh->SetAgentRadius(.34f);
    navMesh->SetPadding(Vector3::UP);
    navMesh->SetAgentMaxClimb(.23f);
    navMesh->SetCellSize(.05f);
    navMesh->SetTileSize(256);
    navMesh->Build();

    unsigned numPlayers{ Max(INPUT->GetNumJoysticks(), 1u * !engineParameters_[EP_HEADLESS].GetBool()) };
//    unsigned numPlayers{ 4 };

    for (unsigned p{ 0u }; p < numPlayers; ++p)
        AddPlayer();

    SoundSource* welcomeSource{ scene_->CreateComponent<SoundSource>() };
    welcomeSource->SetSoundType(SOUND_EFFECT);
    welcomeSource->SetAutoRemoveMode(REMOVE_COMPONENT);
    welcomeSource->Play(GetSample("Welcome"), 0.f, .7f);

    menuMusic_ = GetMusic("Modanung - BulletProof MagiRex");
    gameMusic_ = GetMusic("Alien Chaos - Disorder");
    bossMusic_ = GetMusic("Biomekanik - Symphony of Noises");
    Node* musicNode{ scene_->CreateChild("Music") };
    musicSource_ = musicNode->CreateComponent<SoundSource>();
    musicSource_->SetSoundType(SOUND_MUSIC);
//    GetSubsystem<Audio>()->Stop(); ///////////////////////////////////////////////////////////////////////
}

void MasterControl::SetGameState(const GameState newState)
{
    if (newState != currentState_)
    {
        LeaveGameState();
        currentState_ = newState;
        sinceStateChange_ = 0.f;
        EnterGameState();
    }
}

void MasterControl::SetPaused(bool paused)
{
    paused_ = paused;
    scene_->SetUpdateEnabled(!paused);

    Audio* audio{ GetSubsystem<Audio>() };
    Engine* engine{ GetSubsystem<Engine>() };

    if (paused_)
    {
        audio->PauseSoundType(SOUND_MUSIC);
        engine->SetMaxFps(0);
    }
    else
    {
        audio->ResumeSoundType(SOUND_MUSIC);
        engine->SetMaxFps(GetSubsystem<Graphics>()->GetRefreshRate());
        GetSubsystem<MenuMaster>()->SetVisible(false);
    }
}

void MasterControl::LeaveGameState()
{
    switch (currentState_)
    {
    case GS_INTRO: {} break;
    case GS_LOBBY: {} break;
    case GS_PLAY:
    {
        SPAWN->Deactivate();
    }   break;
    case GS_DEAD:
    {
        world.camera->SetGreyScale(false);
        musicSource_->SetGain(musicSource_->GetGain() / .666f);
    } break;
    case GS_EDIT: break; //Disable EditMaster
    default: break;
    }
}

void MasterControl::EnterGameState()
{
    switch (currentState_)
    {
    case GS_INTRO:
    break;
    case GS_LOBBY:
    {
        SendEvent(E_ENTERLOBBY);

        SPAWN->Clear();

        musicSource_->Play(menuMusic_);

        apple_->Disable();
        heart_->Disable();
        chaoBall_->Disable();
    }
    break;
    case GS_PLAY:
    {
        SendEvent(E_ENTERPLAY);

        musicSource_->Play(gameMusic_);

        apple_->Respawn(true);
        heart_->Respawn(true);
        chaoBall_->Disable();

        world.lastReset = scene_->GetElapsedTime();
        SPAWN->Restart();
    }
    break;
    case GS_DEAD:
    {
        SPAWN->Deactivate();
        world.camera->SetGreyScale(true);
        musicSource_->SetGain(musicSource_->GetGain() * .666f);
    }
    default:
    break;
    }
}

void MasterControl::Eject()
{
    SetGameState(GS_LOBBY);
}

bool MasterControl::AllReady(bool onlyHuman)
{
    if (!players_.Size())
        return false;

    for (Controllable* c: GetSubsystem<InputMaster>()->GetControlled())
    {
        if (c && c->IsInstanceOf<Pilot>())
        {
                if (onlyHuman && c->GetPlayer()->IsHuman())
                    return false;
                else if (!onlyHuman)
                    return false;
        }
    }

    return true;
}

void MasterControl::HandleUpdate(StringHash, VariantMap &eventData)
{

    float timeStep{ eventData[Update::P_TIMESTEP].GetFloat() };

    //Output FPS
    /*
    secondsPerFrame_ *= 1.0f - timeStep;
    secondsPerFrame_ += timeStep * timeStep;
    sinceFrameRateReport_ += timeStep;
    if (sinceFrameRateReport_ >= 1.0f) {
        Log::Write(3, String(1.0f / secondsPerFrame_));
        sinceFrameRateReport_ = 0.0f;
    }
    */
    sinceStateChange_ += timeStep;
    UpdateCursor(timeStep);

    switch (currentState_)
    {
    case GS_LOBBY:
    {
        if (Door::Get()->HidesAllPilots(false))
            Exit();

        if (AllReady(false))
            SetGameState(GS_PLAY);
    }
    break;
    case GS_PLAY: {}
    break;
    case GS_DEAD:
    {
        if (sinceStateChange_ > 5.f && NoHumans())
            SetGameState(GS_LOBBY);
    }
    default:
    break;
    }
}

void MasterControl::UpdateCursor(const float timeStep)
{
    Cursor* cursor{ world.cursor.uiCursor };
    const float lastIdle{ cursor->GetVar("Idle").GetFloat() };
    const float idle{ (Equals(INPUT->GetMouseMove().Length() + Abs(INPUT->GetMouseMoveWheel() + INPUT->GetMouseButtonDown(MOUSEB_ANY)), 0.f)
                ? lastIdle + timeStep
                : Clamp(lastIdle - 5.f * timeStep, 0.f, 2.3f)) };
    cursor->SetVar("Idle", idle);

    Color col{};
    const float a{ Clamp(2.3f - idle, 0.f, 1.f) };
    cursor->SetVisible(!Equals(a, 0.f));
    if (cursor->IsVisibleEffective())
    {
        const float h{ fmod(TIME->GetElapsedTime() * 2.3f, 1.f) };
        const float s{ Clamp(PowN(idle, 2) - 2.3f, 0.f, 1.f) };
        col.FromHSV(h, s, 1.f, a) ;
        cursor->SetColor(col);
    }
}

bool MasterControl::PhysicsRayCast(PODVector<PhysicsRaycastResult>& hitResults, const Ray& ray,
                                   const float distance, const unsigned collisionMask)
{
    if (distance > 1.e-9)
        physicsWorld_->Raycast(hitResults, ray, distance, collisionMask);

    return (hitResults.Size() > 0);
}

bool MasterControl::PhysicsRayCastSingle(PhysicsRaycastResult& hitResult, const Ray& ray,
                                   const float distance, const unsigned collisionMask)
{
    if (distance > 1.e-9)
        physicsWorld_->RaycastSingle(hitResult, ray, distance, collisionMask);

    return hitResult.body_;
}

bool MasterControl::PhysicsSphereCast(PODVector<RigidBody*>& hitResults, const Vector3& center,
                                      const float radius, const unsigned collisionMask)
{
    physicsWorld_->GetRigidBodies(hitResults, Sphere{ center, radius }, collisionMask);

    if (hitResults.Size())
        return true;
    else
        return false;
}

void MasterControl::Exit()
{
    engine_->Exit();
}

float MasterControl::Sine(const float freq, const float min, const float max, const float shift)
{
    float phase{ SinePhase(freq, shift) };
    float add{ .5f * (min + max) };

    return Sin(M_RADTODEG * phase) * .5f * (max - min) + add;
}

float MasterControl::Cosine(const float freq, const float min, const float max, const float shift)
{
    return Sine(freq, min, max, shift + .25f);
}

float MasterControl::SinePhase(float freq, float shift)
{
    return M_PI * 2.f * (freq * scene_->GetElapsedTime() + shift);
}

bool MasterControl::InsideHexagon(const Vector3& position, float radius) const
{
    const Vector3 flatPos{ position * Vector3{ 1.f, 0.f, 1.f } };
    const float boundsCheck{ flatPos.Length() * Cos(flatPos.Angle(GetHexant(flatPos))) };

    if (boundsCheck > radius)
        return false;
    else
        return true;
}

Vector3 MasterControl::GetHexant(const Vector3& position) const
{
    const Vector3 flatPos{ position.ProjectOntoPlane(Vector3::UP) };
    const int sides{ 6 };
    Vector3 hexant{ Vector3::FORWARD };

    for (int h{ 0 }; h < sides; ++h)
    {
        const Vector3 otherHexantNormal{ Quaternion{ h * 360.f / sides, Vector3::UP } * Vector3::FORWARD };
        hexant = flatPos.Angle(otherHexantNormal) < flatPos.Angle(hexant) ? otherHexantNormal : hexant;
    }

    return hexant;
}


Vector<SharedPtr<Player> > MasterControl::GetPlayers()
{
    return players_;
}

Player* MasterControl::GetPlayer(int playerId) const
{
    for (Player* p: players_)
    {
        if (p->GetPlayerId() == playerId)
            return p;
    }

    return nullptr;
}

Player* MasterControl::GetPlayerByColorSet(int colorSet)
{
    for (Ship* s: Ship::ships_)
    {
        if (s->GetColorSet() == colorSet)
            return s->GetPlayer();
    }

    return nullptr;
}

Player* MasterControl::GetNearestPlayer(const Vector3& pos)
{
    InputMaster* im{ GetSubsystem<InputMaster>() };
    Player* nearestPlayer{ nullptr };
    Controllable* nearestControlled{ nullptr };

    for (Player* p: players_)
    {
        Controllable* controlled{ im->GetControllableByPlayer(p->GetPlayerId()) };

        if (controlled && controlled->IsEnabled() && p->IsAlive())
        {
            if (!nearestPlayer ||
                nearestControlled->GetWorldPosition().DistanceToPoint(pos)
                     > controlled->GetWorldPosition().DistanceToPoint(pos))
            {
                nearestPlayer = p;
                nearestControlled = controlled;
            }
        }
    }

    return nearestPlayer;
}

bool MasterControl::AllPlayersScoreZero(bool onlyHuman)
{
    for (Player* p: players_)
    {
        if ((p->IsHuman() || !onlyHuman) && p->GetScore() != 0)
            return false;
    }

    return true;
}

Ship* MasterControl::GetShipByColorSet(int colorSet)
{
    for (Ship* s: Ship::ships_)
    {
        if (s->GetColorSet() == colorSet)
            return s;
    }

    return nullptr;
}

bool MasterControl::NoHumans()
{
    for (Player* p: players_)
    {
        if (p->IsHuman())
            return false;
    }

    return true;
}

void MasterControl::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    physicsWorld_->DrawDebugGeometry(true);
    if (GetGameState() == GS_LOBBY)
        scene_->GetComponent<NavigationMesh>()->DrawDebugGeometry(false);
}

