/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "spawnmaster.h"
#include "mastercontrol.h"

#include "settings.h"

Settings::Settings(Context* context): Object(context),
    settingsScene_{ nullptr },
    settingsCam_{ nullptr },
    panels_{},
    width_{},
    height_{},
    refreshRate_{},
    fullscreen_{ true},
    antiAliasing_{ true },
    manyLights_{ true },
    bloom_{ true },
    vSync_{ true },
    effects_{ true, 1.f },
    music_{ true, .23f }
{
    AUDIO->SetMasterGain(SOUND_EFFECT, effects_.first_ * effects_.second_);
    AUDIO->SetMasterGain(SOUND_MUSIC,  music_  .first_ * music_  .second_);
}

bool Settings::Load()
{
    if (FILES->FileExists(SETTINGSPATH))
    {
        File file{ context_, SETTINGSPATH, FILE_READ };
        XMLFile configFile{ context_ };
        configFile.Load(file);
        XMLElement graphicsElem{ configFile.GetRoot().GetChild("Graphics") };
        XMLElement audioElem{ configFile.GetRoot().GetChild("Audio") };

        if (graphicsElem)
        {
            width_ = graphicsElem.GetInt("Width");
            height_ = graphicsElem.GetInt("Height");
            fullscreen_ = graphicsElem.GetBool("Fullscreen");
            antiAliasing_ = graphicsElem.GetBool("AntiAliasing");
            manyLights_ = graphicsElem.GetBool("ManyLights");
            bloom_ = graphicsElem.GetBool("Bloom");
            vSync_ = graphicsElem.GetBool("VSync");
        }

        if (audioElem)
        {
            // Backwards compatibility
            if (audioElem.HasAttribute("EffectsOn"))
            {
                effects_.first_ = audioElem.GetBool("EffectsOn");
                effects_.second_ = audioElem.GetFloat("EffectsGain");
                music_.first_ = audioElem.GetBool("MusicOn");
            }
            music_.second_ = audioElem.GetFloat("MusicGain");

            AUDIO->SetMasterGain(SOUND_EFFECT, effects_.first_ * effects_.second_);
            AUDIO->SetMasterGain(SOUND_MUSIC,  music_  .first_ * music_  .second_);
        }

        return true;
    }
    else
    {
        return false;
    }
}

void Settings::Save()
{
    XMLFile file{ context_ };
    XMLElement root{ file.CreateRoot("Settings") };

    XMLElement graphicsElement{ root.CreateChild("Graphics") };
    graphicsElement.SetInt("Width", GRAPHICS->GetWidth());
    graphicsElement.SetInt("Height", GRAPHICS->GetHeight());
    graphicsElement.SetBool("Fullscreen", GRAPHICS->GetFullscreen());
    graphicsElement.SetBool("AntiAliasing", antiAliasing_);
    graphicsElement.SetBool("ManyLights", manyLights_);
    graphicsElement.SetBool("Bloom", bloom_);
    graphicsElement.SetBool("VSync", GRAPHICS->GetVSync());

    XMLElement audioElement(root.CreateChild("Audio"));
    audioElement.SetFloat("MusicOn", music_.first_);
    audioElement.SetFloat("MusicGain", music_.second_);
    audioElement.SetFloat("EffectsOn", effects_.first_);
    audioElement.SetFloat("EffectsGain", effects_.second_);

    file.SaveFile(SETTINGSPATH);
}

void Settings::SetSetting(const String& name, const Variant& value)
{
    if (value.GetType() == VAR_BOOL)
    {
        const bool val{ value.GetBool()};

        if (name == "Bloom" || name == "AntiAliasing")
        {
            RenderPath* renderPath{ GetSubsystem<Renderer>()->GetViewport(0u)->GetRenderPath() };

            if (name == "Bloom")
            {
                bloom_ = val;
                renderPath->SetEnabled("BloomHDR", val);

                renderPath->SetShaderParameter("TonemapExposureBias", 3.5f - 1.5f * bloom_);
                renderPath->SetShaderParameter("TonemapMaxWhite",     5.5f + 1.75f * bloom_);
            }
            else if (name == "Anti-Aliasing")
            {
                antiAliasing_ = val;
                renderPath->SetEnabled("FXAA3", val);
            }
        }
        else if (name == "Effects" || name == "Music")
        {
            Audio* audio{ GetSubsystem<Audio>() };
            if (name == "Music")
            {
                if (val != music_.first_)
                {
                    music_.first_ = val;
                    audio->SetMasterGain(SOUND_MUSIC, val ? music_.second_ : 0.f);
                }
            }
            else
            {
                if (val != effects_.first_)
                {
                    effects_.first_ = val;
                    audio->SetMasterGain(SOUND_EFFECT, val ? effects_.second_ : 0.f);
                }
            }
        }
        else if (name == "V-Sync" || name == "Fullscreen")
        {
            if (name == "V-Sync")
            {
                if (val == vSync_)
                    return;

                vSync_ = val;
            }
            else if (name == "Fullscreen")
            {
                if (val == fullscreen_)
                    return;

                fullscreen_ = val;
            }

            Graphics* gfx{ GetSubsystem<Graphics>() };
            gfx->SetMode(width_, height_, fullscreen_,
                         gfx->GetBorderless(), gfx->GetResizable(), gfx->GetHighDPI(),
                         vSync_,
                         gfx->GetTripleBuffer(), gfx->GetMultiSample(), gfx->GetMonitor(), gfx->GetRefreshRate());
        }
        else if (name == "Many lights")
        {
            manyLights_ = val;

            for (Node* node: SPAWN->GetAll())
                if (node && node->HasComponent<Light>())
                    node->GetComponent<Light>()->SetEnabled(val);
        }
    }
    else if (value.GetType() == VAR_FLOAT)
    {
        const float val{ value.GetFloat() };

        if (name == "Effects" || name == "Music")
        {
            Audio* audio{ GetSubsystem<Audio>() };
            if (name == "Music")
            {
                music_.second_ = val;
                audio->SetMasterGain(SOUND_MUSIC, music_.first_ ? val : 0.f);
            }
            else
            {
                effects_.second_ = val;
                audio->SetMasterGain(SOUND_EFFECT, effects_.first_ ? val : 0.f);
            }
        }
    }
    else if (value.GetType() == VAR_INTVECTOR2)
    {
        const IntVector2 resolution{ value.GetIntVector2() };
        if (resolution != GRAPHICS->GetSize())
        {
            width_ = resolution.x_;
            height_ = resolution.y_;
            GRAPHICS->SetMode(width_, height_);
        }
    }
}
