/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "spawnmaster.h"
#include "settings.h"

#include "environment/arena.h"
#include "projectile/bullet.h"
#include "projectile/brick.h"
#include "projectile/seeker.h"
#include "fx/flash.h"
#include "fx/mirage.h"
#include "fx/hitfx.h"

#include "sceneobject.h"

SceneObject::SceneObject(Context* context): LogicComponent(context),
    graphicsNode_{ nullptr },
    sampleSources3D_{},
    blink_{ true },
    big_{ true }
{
    SetUpdateEventMask(USE_NO_EVENT);
}

void SceneObject::Set(const Vector3& position)
{
    StopAllSound();
    node_->SetEnabledRecursive(true);
    node_->SetPosition(position);

    if (blink_)
        SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(SceneObject, HandlePostRenderUpdate));

    if (node_->HasComponent<Light>())
        node_->GetComponent<Light>()->SetEnabled(GetSubsystem<Settings>()->GetManyLights());
}

void SceneObject::Set(const Vector3& position, const Quaternion& rotation)
{
    node_->SetRotation(rotation);
    Set(position);
}

void SceneObject::Disable()
{
//    if (node_->HasComponent<Mirage>())
//        node_->GetComponent<Mirage>()
    node_->SetEnabledRecursive(false);

    if (blink_)
        UnsubscribeFromEvent(E_POSTRENDERUPDATE);

    UnsubscribeFromEvent(E_NODECOLLISIONSTART);
}

void SceneObject::PlaySample(Sound* sample, const float gain, bool localized)
{
//    if (MC->SamplePlayed(sample->GetNameHash().Value()))
//        return;

    if (localized)
    {
        SoundSource3D* sampleSource3D{ GetScene()->CreateChild("SFX")->CreateComponent<SoundSource3D>() };
        sampleSource3D->SetDistanceAttenuation(42.f, 256.f, 2.f);
        sampleSource3D->SetSoundType(SOUND_EFFECT);
        sampleSource3D->SetAutoRemoveMode(REMOVE_NODE);
        sampleSource3D->SetGain(gain);
        sampleSource3D->Play(sample);
    }
    else
    {
        SoundSource* sampleSource{ GetScene()->CreateChild("SFX")->CreateComponent<SoundSource>() };
        sampleSource->SetSoundType(SOUND_EFFECT);
        sampleSource->SetAutoRemoveMode(REMOVE_NODE);
        sampleSource->SetGain(gain);
        sampleSource->Play(sample);
    }
}

void SceneObject::StopAllSound()
{
    for (SoundSource3D* source: sampleSources3D_)
        source->Stop();
}

bool SceneObject::IsPlayingSound()
{
    for (SoundSource3D* source: sampleSources3D_)
        if (source->IsPlaying()) return true;

    return false;
}

void SceneObject::Blink(const Vector3& newPosition, const Vector3& flashOffset)
{
    const Vector3 oldPosition{ GetWorldPosition() };
    node_->SetPosition(newPosition);

//    Player* nearestPlayerA{ MC->GetNearestPlayer(oldPosition) };
//    Player* nearestPlayerB{ MC->GetNearestPlayer(newPosition) };

//    float distanceToNearestPlayer;

//    if (nearestPlayerA && nearestPlayerB)
//        distanceToNearestPlayer = Min(nearestPlayerA->GetPosition().DistanceToPoint(oldPosition),
//                                      nearestPlayerB->GetPosition().DistanceToPoint(newPosition));
//    else
//        distanceToNearestPlayer = 23.0f;

    const float gain{ .042f };//Max(0.07f, 0.13f - distanceToNearestPlayer * 0.0023f) };

    SPAWN->Create<Flash>()->Set(oldPosition + flashOffset, 0.f, big_);
    SPAWN->Create<Flash>()->Set(newPosition + flashOffset, gain, big_);
}

void SceneObject::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (MC->IsPaused())
        return;

    const Vector3 position{ node_->GetPosition() };
    const float radius{ ARENA_RADIUS };

    if (!MC->InsideHexagon(position, radius))
    {
        if (node_->HasComponent<Bullet>()
         || node_->HasComponent<Seeker>()
         || node_->HasComponent<Brick>())
        {
            HitFX* hitFx{ SPAWN->Create<HitFX>() };
            hitFx->Set(position, 0, false);
            Disable();
        }
        else
        {
            const Vector3 newPosition{ position - 1.95f * radius * MC->GetHexant(position) };
            Blink(newPosition);
        }
    }
}

void SceneObject::Emerge(const float timeStep)
{
    if (!IsEmerged())
    {
        node_->Translate(2.3f * Vector3::UP * timeStep *
                         (.023f - node_->GetPosition().y_),
                         TS_WORLD);
    }
}
