/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BAPHOMECH_H
#define BAPHOMECH_H

#include "enemy.h"

class Baphomech: public Enemy
{
    DRY_OBJECT(Baphomech, Enemy);

#define VERTEBRA_NUM 9u
#define VERTEBRA_OFFSET (Vector3::UP * 1.14f)

public:
    static void RegisterObject(Context* context);
    Baphomech(Context* context);

    void Start() override;
    void FixedUpdate(float timeStep) override;
    void Update(float timeStep) override;
    void Set(const Vector3& position) override;
    void Disable() override;
    void Hit(float damage, int colorSet) override;
    void HandleNodeCollision(StringHash eventType, VariantMap &eventData) override;

private:

    void Shoot();

    SharedPtr<Material> barMaterial_;
    SharedPtr<Material> spikeMaterial_;
    SoundSource3D* engineSource_;
    Node* healthBar_;

    Node* spineRoot_;
    Vector<Node*> spine_;
    Vector3 spineOrigin_;
    const Vector3 spineStartOrigin{ -2.0f * VERTEBRA_NUM * VERTEBRA_OFFSET + Vector3::FORWARD * ARENA_RADIUS };

    Vector3 targetPosition_;
    float jawAngle_;
    float targetJawAngle_;
    Vector3 jawHitNormal_;

    int toSwitch_;
    int toShoot_;
    int maxShots_;
    float sinceShot_;
    float shotInterval_;
    float time_;

    void DrawDebug(StringHash, VariantMap&);
    void UpdateJawMotors(float timeStep);
    void Razornado(float timeStep);
};

#endif // BAPHOMECH_H
