/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../player/player.h"
#include "../projectile/seeker.h"
#include "../spawnmaster.h"

#include "spire.h"

void Spire::RegisterObject(Context* context)
{
    context->RegisterFactory<Spire>();
}

Spire::Spire(Context* context): Enemy(context),
    initialShotInterval_{ 2.3f },
    shotInterval_{ initialShotInterval_ },
    sinceLastShot_{ 0.f }
{
}

void Spire::Start()
{
    Enemy::Start();

    health_ = initialHealth_ = 5.f;
    worth_ = 10;

    rigidBody_->SetMass(3.f);
    rigidBody_->SetLinearFactor(Vector3::ZERO);

    SharedPtr<Material> black{ RES(Material, "Materials/Spire.xml")->Clone() };

    topNode_ = node_->CreateChild("SpireTop");
    topModel_ = topNode_->CreateComponent<StaticModel>();
    topModel_->SetModel(RES(Model, "Models/SpireTop.mdl"));
    topModel_->SetMaterial(black);

    bottomNode_ = node_->CreateChild("SpireBottom");
    bottomModel_ = bottomNode_->CreateComponent<StaticModel>();
    bottomModel_->SetModel(RES(Model, "Models/SpireBottom.mdl"));
    bottomModel_->SetMaterial(black);
}

Seeker* Spire::Shoot(bool sound)
{
    sinceLastShot_ = 0.f;
    Seeker* seeker{ SPAWN->Create<Seeker>() };
    seeker->Set(node_->GetPosition(), sound);

    return seeker;
}

void Spire::Update(float timeStep)
{
    if (!node_->IsEnabled()) return;

    Enemy::Update(timeStep);
    //Pulse
    topModel_->GetMaterial()->SetShaderParameter("MatEmissiveColor", GetGlowColor());
    //Spin
    const float spinVelocity{ 50.f + panic_ * 300.f };
    topNode_->Rotate(Quaternion{ 0.f, timeStep * spinVelocity, 0.f });
    bottomNode_->Rotate(Quaternion{ 0.f, timeStep * -spinVelocity, 0.f });

    //Shoot
    if (MC->GetGameState() == GS_PLAY && IsEmerged())
    {
        sinceLastShot_ += timeStep;

        if (sinceLastShot_ > shotInterval_)
            Shoot();
    }
}

void Spire::Hit(float damage, int ownerID)
{
    Enemy::Hit(damage, ownerID);

    shotInterval_ = initialShotInterval_ - panic_;
}

void Spire::Set(const Vector3& position)
{
    Enemy::Set(position);

    shotInterval_ = initialShotInterval_;
}
