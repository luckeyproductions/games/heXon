/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "razor.h"

#include "../fx/mirage.h"
#include "../fx/phaser.h"
#include "../spawnmaster.h"

void Razor::RegisterObject(Context *context)
{
    context->RegisterFactory<Razor>();
}

Razor::Razor(Context* context): Enemy(context),
    topSpeed_{ 10.f },
    aimSpeed_{ .25f * topSpeed_ },
    spinRate_{}
{
    sprite_ = true;
    meleeDamage_ = .9f;
}

void Razor::Start()
{
    Enemy::Start();

    rigidBody_->SetMass(2.f);

    if (!sprite_)
    {
        SharedPtr<Material> black{ RES(Material, "Materials/Mason.xml")->Clone() };
        topNode_ = node_->CreateChild("RazorTop");
        topModel_ = topNode_->CreateComponent<StaticModel>();
        topModel_->SetModel(RES(Model, "Models/RazorHalf.mdl"));
        topModel_->SetMaterial(0, RES(Material, "Materials/Mason.xml"));
        topModel_->SetMaterial(1, centerModel_->GetMaterial());

        bottomNode_ = node_->CreateChild("RazorBottom");
        bottomNode_->SetRotation(Quaternion{ 180.f, Vector3::RIGHT });
        bottomModel_ = bottomNode_->CreateComponent<StaticModel>();
        bottomModel_->SetModel(RES(Model, "Models/RazorHalf.mdl"));
        bottomModel_->SetMaterial(0, black);
        bottomModel_->SetMaterial(1, centerModel_->GetMaterial());
    }
    else
    {
        AnimatedBillboardSet* sprite{ node_->CreateComponent<AnimatedBillboardSet>() };
        SharedPtr<Material> mat{ RES(Material, "Materials/RazorSprite.xml")->Clone() };

        sprite->SetNumBillboards(1);
        sprite->SetMaterial(mat);
        sprite->SetSorted(true);
        sprite->SetRelative(true);
        sprite->SetSpeed(10.f);

        for (Billboard& bb: sprite->GetBillboards())
        {
            bb.size_ = Vector2{ 1.17f, 1.17f };
            bb.enabled_ = true;
        }

        sprite->LoadFrames(RES(XMLFile, "Textures/Mirage.xml"));
        sprite->Commit();
    }

    rigidBody_->SetLinearRestThreshold(.0023f);

    Mirage* mirage{ node_->CreateComponent<Mirage>() };
    mirage->SetColor(color_ * .9f);
    mirage->SetSize(.9f);
}

void Razor::Set(const Vector3& position)
{
    aimSpeed_ = .25f * topSpeed_;
    Enemy::Set(position);

    GetComponent<Mirage>()->SetColor(color_ * .9f);
}

void Razor::Update(float timeStep)
{
    if (!node_->IsEnabled())
        return;

    Enemy::Update(timeStep);

    //Spin
    spinRate_ = Max(5.f, 55.f * aimSpeed_ - 17.f * rigidBody_->GetLinearVelocity().Length()) ;

    if (!sprite_) {

        topNode_->Rotate(Quaternion{ 0.f, timeStep * spinRate_, 0.f });
        bottomNode_->Rotate(Quaternion{ 0.f, timeStep * spinRate_, 0.f });
        //Pulse
        topModel_->GetMaterial(0)->SetShaderParameter("MatEmissiveColor", GetGlowColor());

    } else {

        smokeNode_->SetScale(.75f);

        AnimatedBillboardSet* sprite{ GetComponent<AnimatedBillboardSet>() };
        sprite->GetMaterial()->SetShaderParameter("MatEmissiveColor", (color_ + GetGlowColor()) * .42f);
        sprite->SetSpeed(spinRate_ * .042f);
        sprite->GetBillboard(0)->size_ = Vector2{ 1.23f, 1.23f + .023f * Max(0.f, node_->GetWorldPosition().z_ / -5.f) };
        sprite->Commit();
    }
}

void Razor::FixedUpdate(float timeStep)
{
    //Get moving
    if (rigidBody_->GetLinearVelocity().Length() < rigidBody_->GetLinearRestThreshold() && IsEmerged())
    {
        rigidBody_->ApplyForce(timeStep * 42.f * (Quaternion{ Random(6) * 60.f, Vector3::UP } * Vector3::FORWARD));
    }
    //Adjust speed
    else if (rigidBody_->GetLinearVelocity().Length() < aimSpeed_)
    {
        rigidBody_->ApplyForce(timeStep * 235.f * rigidBody_->GetLinearVelocity().Normalized() * Max(aimSpeed_ - rigidBody_->GetLinearVelocity().Length(), .1f));
    }
    else
    {
        float overSpeed{ rigidBody_->GetLinearVelocity().Length() - aimSpeed_ };
        rigidBody_->ApplyForce(timeStep * 100.f * -rigidBody_->GetLinearVelocity() * Sqrt(Max(0.f, overSpeed)));
    }

    Enemy::FixedUpdate(timeStep);
}

void Razor::Hit(float damage, int ownerID)
{
    Enemy::Hit(damage, ownerID);
    aimSpeed_ = (.25f + .75f * panic_) * topSpeed_;
}

void Razor::Blink(const Vector3& newPosition, const Vector3& /*flashOffset*/)
{
    if (!sprite_)
    {
        for (StaticModel* sm: { topModel_, bottomModel_ })
        {
            Phaser* phaser{ SPAWN->Create<Phaser>() };
            phaser->Set(sm->GetModel(), GetWorldPosition(), node_->GetRotation(), rigidBody_->GetLinearVelocity(), Quaternion(spinRate_, Vector3::UP));
        }
    }

    SceneObject::Blink(newPosition);
}
