/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../effectmaster.h"
#include "../projectile/brick.h"

#include "mason.h"

void Mason::RegisterObject(Context* context)
{
    context->RegisterFactory<Mason>();
    context->MC->GetSample("Brick");
}

Mason::Mason(Context* context): Enemy(context),
    shots_{ 0 },
    sinceShot_{ 0.f },
    shotInterval_{ .23f },
    spinInterval_{ 2.3f },
    toSpin_{ 0.f },
    spun_{ 0.f }
{
}

void Mason::Start()
{
    Enemy::Start();

    meleeDamage_ = .23f;
    health_ = initialHealth_ = 15.f;
    worth_  = 42;

    rigidBody_->SetMass(3.4f);
    rigidBody_->SetLinearFactor(Vector3::ZERO);

    blackMaterial_ = RES(Material, "Materials/Mason.xml")->Clone();
    glowMaterial_  = RES(Material, "Materials/Mason.xml")->Clone();
    glowMaterial_->SetShaderParameter("MatEmissiveColor", color_);

    topNode_ = node_->CreateChild("MasonTop");
    topNode_->Rotate(Quaternion{ 30.f, Vector3::UP });
    StaticModel* topModel{ topNode_->CreateComponent<StaticModel>() };
    topModel->SetModel(RES(Model, "Models/MasonTop.mdl"));
    topModel->SetMaterial(0, blackMaterial_);
    topModel->SetMaterial(1, glowMaterial_);

    bottomNode_ = node_->CreateChild("MasonBottom");
    bottomNode_->Rotate(Quaternion{ 30.f, Vector3::UP });
    StaticModel* bottomModel{ bottomNode_->CreateComponent<StaticModel>() };
    bottomModel->SetModel(RES(Model, "Models/MasonBottom.mdl"));
    bottomModel->SetMaterial(0, blackMaterial_);
    bottomModel->SetMaterial(1, glowMaterial_);
}

void Mason::Set(const Vector3& position)
{
    Enemy::Set(position);

    shots_ = 0;
    sinceShot_ = toSpin_ = spun_ = 0.f;
    shotInterval_ = .23f;
    spinInterval_ = 2.3f;

    const float rot{ Random(3) * 60.f + 30.f };

    topNode_->SetRotation(Quaternion{ rot, Vector3::UP });
    bottomNode_->SetRotation(Quaternion{ rot, Vector3::UP });
}

void Mason::Update(float timeStep)
{
    if (!node_->IsEnabled())
        return;

    Enemy::Update(timeStep);

    blackMaterial_->SetShaderParameter("MatEmissiveColor", GetGlowColor());

    if (!IsEmerged())
        return;

    sinceShot_ += timeStep;

    if (sinceShot_ > shotInterval_)
    {
        if (shots_ != 0 && shots_ < MaxShots())
            Shoot();
        else
            shots_ = 0;
    }

    if (sinceShot_ > spinInterval_)
    {
        if (toSpin_ == 0.f)
        {
            shots_ = 0;
            spun_ = 0.f;

            toSpin_ = 360.f * Random(5, 8 - FloorToInt(panic_ * 3))
                     + 60.f * Random(3);
        }

        float spinVelocity{ 666.f };

        if (Min(spun_, toSpin_) < 180.f)
            spinVelocity *= .01f + Min(spun_, toSpin_) / 180.f;

        const float rotDelta{ Min(spinVelocity * timeStep, toSpin_) };
        toSpin_ -= rotDelta;
        spun_ += rotDelta;

        topNode_->Rotate(Quaternion{ rotDelta, Vector3::UP });
        bottomNode_->Rotate(Quaternion{ 2.f * rotDelta, Vector3::DOWN });

        if (toSpin_ == 0.f)
        {
            shotInterval_ = .23f - panic_ * .1f;
            Shoot();
        }
    }
}

void Mason::Shoot()
{
    PlaySample(MC->GetSample("Brick_s"), .14f, false);

    SPAWN->Create<Brick>()->Set(GetWorldPosition(), topNode_->GetDirection());
    SPAWN->Create<Brick>()->Set(GetWorldPosition(), -topNode_->GetDirection());

    sinceShot_ = 0.f;
    ++shots_;

    PlaySample(MC->GetSample("Brick"), .34f);
}

int Mason::MaxShots()
{
    return FloorToInt(panic_ * 10) + 3;
}
