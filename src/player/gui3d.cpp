/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../effectmaster.h"
#include "../inputmaster.h"
#include "../spawnmaster.h"
#include "../fx/line.h"
#include "../environment/lobby.h"
#include "../environment/panel.h"
#include "ship.h"
#include "player.h"

#include "gui3d.h"

void GUI3D::RegisterObject(Context* context)
{
    context->RegisterFactory<GUI3D>();
    context->MC->GetSample("Death");
}

GUI3D::GUI3D(Context* context): LogicComponent(context),
    colorSet_{},
    score_{},
    toCount_{},
    health_{},
    appleCount_{},
    heartCount_{},
    barrelCount_{ 1 },
    cannonNode_{ nullptr },
    healthIndicator_{},
    deathSource_{},
    subNode_{},
    scoreNode_{},
    scoreDigits_{},
    healthBarNode_{},
    healthBarModel_{},
    shieldBarNode_{},
    shieldBarModel_{},
    appleCounterRoot_{},
    appleCounter_{},
    heartCounterRoot_{},
    heartCounter_{},
    barrels_{},
    abilityIcons_{},
    abilitySheet_{ context_ },
    abilitiesTexture_{ nullptr },
    lastCharges_{}
{
}

void GUI3D::Start()
{
    SubscribeToEvent(E_ENTERLOBBY, DRY_HANDLER(GUI3D, EnterLobby));
    SubscribeToEvent(E_ENTERPLAY,  DRY_HANDLER(GUI3D, EnterPlay));
    SubscribeToEvent(E_SCREENMODE,  DRY_HANDLER(GUI3D, HandleScreenMode));
}

void GUI3D::Initialize(int colorSet)
{
    colorSet_ = colorSet;

    subNode_ =  node_->CreateChild("Sub");
    subNode_->SetPosition(LOBBYPOS);
//    subNode_->SetScale(MC->AspectRatio() / 1.7f);
    /*  if (MC->GetAspectRatio() < 1.6f) {
          subNode_->Translate(Vector3( (colorSet_ / 2 == 1) ? -0.9f : 0.9f,
                                      0.0f, 0.0f));
      }*/

    float angle{};
    switch(colorSet_)
    {
    case 1: angle =  -60.f; break;
    case 2: angle =   60.f; break;
    case 3: angle = -120.f; break;
    case 4: angle =  120.f; break;
    }

//    subNode_->Rotate(Quaternion(180.0f * (colorSet_ - 1 % 2), Vector3::FORWARD));
    node_->Rotate(Quaternion{ angle, Vector3::UP });

    CreateScoreCounter();
    CreateHealthBar();
    CreateItemCounters();
    CreateCannon();
    CreateAbilityIcons();

    MC->scene_->CreateChild("Panel")->CreateComponent<Panel>()->Initialize(colorSet_);

    deathSource_ = node_->CreateComponent<SoundSource>();
    deathSource_->SetSoundType(SOUND_EFFECT);
    deathSource_->SetGain(2.3f);
}

void GUI3D::CreateScoreCounter()
{
    scoreNode_ = subNode_->CreateChild("Score");
    if (colorSet_ == 4)
        scoreNode_->Rotate(Quaternion(180.f, 0.f, 180.f));

    for (int d{ 0 }; d < 10; ++d)
    {
        scoreDigits_[d] = scoreNode_->CreateChild("Digit");
        scoreDigits_[d]->SetEnabled( d == 0 );
        scoreDigits_[d]->Translate(Vector3::RIGHT * (colorSet_ == 2 ? -.5f : .5f) * (d - 4.5f));
        scoreDigits_[d]->Rotate(Quaternion{ colorSet_ == 2 ? 0.f : 180.f, Vector3::UP }, TS_WORLD);
        scoreDigits_[d]->Rotate(Quaternion{ (colorSet_ % 2) * 90.f - 45.f, Vector3::RIGHT }, TS_LOCAL);
        scoreDigits_[d]->SetScale(.9f);

        StaticModel* digitModel{ scoreDigits_[d]->CreateComponent<StaticModel>() };
        digitModel->SetModel(RES(Model, "Models/0.mdl"));
        digitModel->SetMaterial(MC->colorSets_[colorSet_].glowMaterial_);
//        digitModel->SetLightMask(0);
    }
}

void GUI3D::CreateHealthBar()
{
    healthIndicator_ = subNode_->CreateComponent<AnimatedModel>();
    healthIndicator_->SetModel(RES(Model, "Models/HealthIndicator.mdl"));
    healthIndicator_->SetMaterial(0, RES(Material, "Materials/GreenGlow.xml")->Clone());
    healthIndicator_->SetMaterial(1, RES(Material, "Materials/BlueGlow.xml"));
    healthIndicator_->SetMaterial(2, RES(Material, "Materials/Black.xml"));
    healthIndicator_->SetMorphWeight(0, 1.f);
    healthIndicator_->SetMorphWeight(1, 0.f);
}

void GUI3D::CreateItemCounters()
{
    appleCounterRoot_ = subNode_->CreateChild("AppleCounter");
//    appleCounterRoot_->Rotate(Quaternion(180.0f * ((colorSet_ - 1) % 2), Vector3::FORWARD));

    for (int a{ 0 }; a < 4; ++a)
    {
        appleCounter_[a] = appleCounterRoot_->CreateChild();
        appleCounter_[a]->SetEnabled(false);

        if ((colorSet_ - 1) % 2)
            appleCounter_[a]->SetPosition(Vector3::FORWARD * 2.3f + Vector3::RIGHT * (a * -.666f + 1.f));
        else
            appleCounter_[a]->SetPosition(Vector3::FORWARD * 2.3f + Vector3::RIGHT * (a *  .666f - 1.f));

        appleCounter_[a]->SetScale(.23f);
        appleCounter_[a]->Rotate(Quaternion{ 13.f, Vector3::RIGHT }, TS_WORLD);
        StaticModel* apple{ appleCounter_[a]->CreateComponent<StaticModel>() };
        apple->SetModel(RES(Model, "Models/Apple.mdl"));
        apple->SetMaterial(RES(Material, "Materials/GoldEnvmap.xml"));
    }

    heartCounterRoot_ = subNode_->CreateChild("HeartCounter");
//    heartCounterRoot_->Rotate(Quaternion(180.0f * ((colorSet_ - 1) % 2), Vector3::FORWARD));

    for (int h{ 0 }; h < 4; ++h)
    {
        heartCounter_[h] = heartCounterRoot_->CreateChild();
        heartCounter_[h]->SetEnabled(false);

        if ((colorSet_ - 1) % 2)
            heartCounter_[h]->SetPosition(Vector3::FORWARD * 2.3f + Vector3::RIGHT * (h * -.666f + 1.f));
        else
            heartCounter_[h]->SetPosition(Vector3::FORWARD * 2.3f + Vector3::RIGHT * (h *  .666f - 1.f));

        heartCounter_[h]->SetScale(.23f);
        heartCounter_[h]->Rotate(Quaternion{ 13.f, Vector3::RIGHT }, TS_WORLD);
        StaticModel* heart{ heartCounter_[h]->CreateComponent<StaticModel>() };
        heart->SetModel(RES(Model, "Models/Heart.mdl"));
        heart->SetMaterial(RES(Material, "Materials/RedEnvmap.xml"));
    }
}

void GUI3D::CreateCannon()
{
    cannonNode_ = subNode_->CreateChild("Cannon");
    cannonNode_->SetPosition({ 2.9f * ((colorSet_ & 2) - 1), -.13f, 1.55f });
    float parentZ{ cannonNode_->GetParent()->GetWorldPosition().z_ };
    cannonNode_->LookAt(Vector3::BACK * parentZ * 2.7f + Vector3::DOWN * .55f, Vector3::BACK * parentZ * 1.f + Vector3::UP * 8.f);
//    cannonNode_->SetRotation(Quaternion(-90.0f, Vector3::UP));

    StaticModel* cannonBase{ cannonNode_->CreateComponent<StaticModel>() };
    cannonBase->SetModel(RES(Model, "Models/CannonBase.mdl"));
    cannonBase->SetMaterial(0, MC->colorSets_[colorSet_].hullMaterial_);
    cannonBase->SetMaterial(1, MC->colorSets_[colorSet_].glowMaterial_);

    for (int c{ 0 }; c < 5; ++c)
    {
        Node* barrelNode{ cannonNode_->CreateChild("Barrel") };

        float z{ -.035202f };
        float x{ 0.f };
        const bool front{ c < 3 };
        const bool left{ c % 2 != 0 };

        if (c != 0)
        {
            float rot{ front ? 17.f : -17.f };
            x = front ? .136274 : .098474;

            if (left)
            {
                x = -x;
                rot = - rot;
            }

            if (!front)
                rot += 180.f;

            z = front ? -.055623f : -.049149f;

            barrelNode->Rotate(Quaternion{ rot, Vector3::UP });
        }

        barrelNode->SetPosition(Vector3{ x, 0.f, z });

        AnimatedModel* barrel{ barrelNode->CreateComponent<AnimatedModel>() };
        barrel->SetModel(RES(Model, "Models/CannonBarrel.mdl"));
        barrel->SetMaterial(0, MC->colorSets_[colorSet_].hullMaterial_);
        barrel->SetMaterial(1, RES(Material, "Materials/Metal.xml"));
        barrel->SetMaterial(2, RES(Material, "Materials/Black.xml"));
        barrels_.Push(barrel);

        if (c == 0)
        {
            for (unsigned m{ 0 }; m < barrel->GetNumMorphs(); ++m)
                barrel->SetMorphWeight(m, 1.f);
        }
    }
}

void GUI3D::CreateAbilityIcons()
{
    Node* abilitiesNode{ subNode_->CreateChild("Abilities") };
    StaticModel* abilitiesPlane{ abilitiesNode->CreateComponent<StaticModel>() };
    abilitiesPlane->SetModel(RES(Model, "Models/Plane.mdl"));
    abilitiesPlane->SetMaterial(RES(Material, "Materials/Add.xml")->Clone());
    abilitiesNode->SetPosition({ 3.4f * ((colorSet_ & 2) - 1), 0.f, 0.f });
    abilitiesNode->SetScale(.8f * Vector3{ 1.5f, 1.f, 1.f });
    abilitiesNode->Pitch(-45.f);

    abilitySheet_.SetSize({ 512, 512 }, 4u);
    abilitySheet_.Clear(Color::TRANSPARENT_BLACK);
    SharedPtr<Texture2D> abilitiesTexture{ RES(Texture2D, "UI/Abilities.png") };
    SharedPtr<Image> abilitiesImage{ abilitiesTexture->GetImage() };

    for (int a{ 0 }; a < 4; ++a)
    {
        const int s{ 128 };
        const IntVector2 iconSize{ s, s };
        IntRect iconRect{};
        switch (a)
        {
        case REPEL:       iconRect = {     0,     0, 1 * s, 1 * s }; break;
        case DIVE:        iconRect = { 1 * s,     0, 2 * s, 1 * s }; break;
        case RAM:         iconRect = {     0, 1 * s, 1 * s, 2 * s }; break;
        case DEPTHCHARGE: iconRect = { 1 * s, 1 * s, 2 * s, 2 * s }; break;
        default: continue;
        }

        SharedPtr<Image> icon{ new Image{ context_ } };
        icon->SetSize({ 200, 256 }, 4u);
        icon->Clear(0);
        icon->SetSubimage(abilitiesImage->GetSubimage(iconRect), { {}, {icon->GetWidth(), icon->GetHeight()} });
        SharedPtr<Image> iconBase{ new Image{ context_ } };
        iconBase->SetSize({ 200, 256 }, 4u);
        iconBase->Clear(0);
        iconBase->SetSubimage(abilitiesImage->GetSubimage(iconRect), { {}, {icon->GetWidth(), icon->GetHeight()} });
        iconBase->MultiplyAlpha(.23f);

        Pair<SharedPtr<Image>, SharedPtr<Image>> iconPair{ iconBase, icon };

        abilityIcons_.Insert({ a, iconPair });
    }

    abilitiesTexture_ = MakeShared<Texture2D>(context_);
    abilitiesTexture_->SetNumLevels(1);
    abilitiesTexture_->SetMipsToSkip(QUALITY_LOW, 0);

    Material* abilitiesMaterial{ abilitiesPlane->GetMaterial() };
    Color col{ MC->colorSets_[colorSet_].colors_.first_ };
    col.FromHSV(col.Hue(), col.SaturationHSV(), .75f);
    abilitiesMaterial->SetShaderParameter("MatDiffColor", col);
    abilitiesMaterial->SetTexture(TU_DIFFUSE, abilitiesTexture_);

    UpdateAbilityIcons();
}

void GUI3D::Update(float timeStep)
{
//    SetBarrels(((static_cast<int>(TIME->GetElapsedTime()) / 2) % 5) + 1);

    CountScore();
    UpdateAbilityIcons();

    for (int i{ 0 }; i < 4; ++i)
    {
        appleCounter_[i]->Rotate(Quaternion{ .0f, (i * i + 10.f) * 23.f * timeStep, 0.f });
        appleCounter_[i]->SetScale(MC->Sine((.23f + (appleCount_)) * .23f, .1f, .17f, -i * 2.3f));
        heartCounter_[i]->Rotate(Quaternion{ .0f, (i * i + 10.f) * 23.f * timeStep, 0.f });
        heartCounter_[i]->SetScale(MC->Sine((.23f + (heartCount_)) * .23f, .1f, .17f, -i * 2.3f));
    }

    //Update HealthBar color
    healthIndicator_->GetMaterial(0)->SetShaderParameter("MatDiffColor", HealthToColor(health_));
    healthIndicator_->GetMaterial(0)->SetShaderParameter("MatEmissiveColor", HealthToColor(health_) * .42f);
    healthIndicator_->GetMaterial(0)->SetShaderParameter("MatSpecularColor", HealthToColor(health_) * .05f);

    for (unsigned b{ 0 }; b < barrels_.Size(); ++b)
    {
        AnimatedModel* barrel{ barrels_[b] };
        float weight{};

        if (b == 0)
        {
            if (!(barrelCount_ & 2))
                weight = 1.f;
        }
        else if (b < 3)
        {
            if (barrelCount_ > 1)
                weight = 1.f;
        }
        else
        {
            if (barrelCount_ > 2)
                weight = 1.f;
        }

        for (unsigned m{ 0 }; m < barrel->GetNumMorphs(); ++m)
        {
            if (weight == 1.f)
                barrel->SetMorphWeight(m, Lerp(barrel->GetMorphWeight(m), m == 0 ? weight : (barrel->GetMorphWeight(m - 1) > .99f), Min(1.f, timeStep * 17.f)));
            else
                barrel->SetMorphWeight(m, Lerp(barrel->GetMorphWeight(m), (m == barrel->GetNumMorphs() - 1) ? weight : (barrel->GetMorphWeight(m + 1) > .01f), Min(1.f, timeStep * 17.f)));
        }
    }
}

void GUI3D::PlayDeathSound()
{
    deathSource_->Play(MC->GetSample("Death"));
}

void GUI3D::SetHealth(float health)
{
    health_ = health;

    healthIndicator_->SetMorphWeight(0, Min(health_, 10.f) * .1f);
    healthIndicator_->SetMorphWeight(1, Max(health_ - 10.f,  .0f) * .2f);

//    healthBarNode_->SetScale(Vector3(Min(health_, 10.0f), 1.0f, 1.0f));
//    shieldBarNode_->SetScale(Vector3(health_, 0.95f, 0.95f));
}

void GUI3D::SetHeartsAndApples(int hearts, int apples)
{
    appleCount_ = apples;
    heartCount_ = hearts;

    for (int a{ 0 }; a < 4; ++a)
        appleCounter_[a]->SetEnabled(appleCount_ > a);

    for (int h{ 0 }; h < 4; ++h)
        heartCounter_[h]->SetEnabled(heartCount_ > h);
}

void GUI3D::SetScore(unsigned score)
{
    if (score > score_)
        toCount_ += score - score_;

    score_ = score;
    //Update score graphics
    for (unsigned d{ 0u }; d < 10u; ++d)
    {
        const unsigned p{ PowN(10u, d) };
        StaticModel* digitModel{ scoreDigits_[d]->GetComponent<StaticModel>() };
        digitModel->SetModel(RES(Model, "Models/" + String((score_ / p) % 10u ) + ".mdl"));
        scoreDigits_[d]->SetEnabled(score_ >= p || d == 0);
    }
}

void GUI3D::SetBarrels(int num)
{
    barrelCount_ = num;
}

void GUI3D::UpdateAbilityIcons(bool chargeRelevant)
{
    Ship* ship{ MC->GetShipByColorSet(colorSet_) };
    if (!ship)
        return;
    HashMap<int, float> charges{};
    for (int a{ 0 }; a < 4; ++a)
        charges[a] = ship->GetActionCharge(a);
    if (chargeRelevant && charges == lastCharges_)
        return;

    abilitySheet_.Clear(Color::TRANSPARENT_BLACK);

    const IntVector2 iconGridSize{ 200, 256 };
    for (int a{ 0 }; a < 4; ++a)
    {
        if (!ship->HasAbility(a))
            continue;

        const float charge{ charges[a] };
        const bool flipOffset{ colorSet_ == 2 || colorSet_ == 3 };
        const bool secondRow{ a > DIVE };
        const bool secondColumn{ a % 2 == 1 };
        const IntVector2 iconPosition{ iconGridSize.x_ * (2 * secondColumn - (secondRow ^ flipOffset) + 1) / 2,
                                       iconGridSize.y_ * secondRow };

        for (bool base: { true, false })
        {

            const Image* const icon{ (base ? abilityIcons_[a].first_ : abilityIcons_[a].second_) };
            if (!icon)
                continue;

            const int h{ RoundToInt(icon->GetHeight() * charge) };
            if (base && h == icon->GetHeight())
                continue;
            else if (!base && h == 0)
                continue;

            const int top{    (base ? 0                     : icon->GetHeight() - h) };
            const int bottom{ (base ? icon->GetHeight() - h : icon->GetHeight())     };
            SharedPtr<Image> iconPart{ icon->GetSubimage({ { 0, top },
                                                           { icon->GetWidth(), bottom } }) };
            if (!iconPart)
                continue;

            const IntVector2 iconPartSize{ iconPart->GetWidth(), iconPart->GetHeight() };
            const IntVector2 iconPartPosition{ iconPosition.x_, (base ? iconPosition.y_ : (iconPosition.y_ + iconGridSize.y_ - h))};
            abilitySheet_.SetSubimage(iconPart, { iconPartPosition,
                                                  iconPartPosition + iconPartSize });
        }
    }

    SubscribeToEvent(E_BEGINRENDERING, DRY_HANDLER(GUI3D, UpdateAbilitiesTexture));
    lastCharges_ = charges;
}

void GUI3D::UpdateAbilitiesTexture(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    abilitiesTexture_->SetData(&abilitySheet_);
    UnsubscribeFromEvent(E_BEGINRENDERING);
}

void GUI3D::CountScore()
{
    Player* player{ MC->GetPlayerByColorSet(colorSet_) };

    if (!player || !player->IsAlive())
        return;

    int maxLines{ 512 };
    int threshold{ maxLines / (Max(SPAWN->CountActive<Ship>(), 1) * 8) };

    int lines{ SPAWN->CountActive<Line>() };
    int counted{};

    while (toCount_ > 0
        &&    lines < maxLines
        &&  counted < threshold)
    {
        SPAWN->Create<Line>()->Set(colorSet_);
        --toCount_;
        ++lines;
        ++counted;
    }
}

void GUI3D::EnterLobby(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    node_->SetEnabledRecursive(true);
    SetHeartsAndApples(0, 0);
    SetScore(score_);

    health_ = 0.0f;
    healthIndicator_->SetMorphWeight(0, 1.f);
    healthIndicator_->SetMorphWeight(1, 0.f);

    node_->SetPosition(Vector3::UP);
    node_->SetScale(1.f);
    subNode_->SetScale(.75f);

    toCount_ = 0;
    cannonNode_->SetEnabledRecursive(false);
}

void GUI3D::EnterPlay(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (!MC->GetPlayerByColorSet(colorSet_))
    {
        node_->SetEnabledRecursive(false);
        return;
    }

    SetHeartsAndApples(0, 0);
    SetScore(score_);

    node_->SetPosition(Vector3::DOWN * 1.23f);
    node_->SetScale(3.6f);

    cannonNode_->SetEnabledRecursive(true);
}

void GUI3D::HandleScreenMode(StringHash /*eventType*/, VariantMap& eventData)
{
//    const float ratio{ MC->AspectRatio() };
//    subNode_->SetScale(ratio / 1.7f);
    lastCharges_ = {};
}

Color GUI3D::HealthToColor(float health)
{
    Color color{ .23f, 1.f, .05f, 1.f };
    health = Clamp(health, 0.f, 10.f);
    float maxBright{ health < 5.f ? MC->Sine(.75f + .24f * (4.f - health), .25f * health, .666f)
                                  : .42f };

    color.r_ = Clamp((3.f - (health - 3.f)) / 2.3f, 0.f, maxBright);
    color.g_ = Clamp((health - 3.f) / 4.f, 0.f, maxBright);

    return color;
}
