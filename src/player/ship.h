/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SHIP_H
#define SHIP_H

#include "../inputmaster.h"

#include "controllable.h"

class Bullet;
class Muzzle;
class GUI3D;
class Trail;
class DiveBubbles;

class Ship: public Controllable
{
    DRY_OBJECT(Ship, LogicComponent);

public:
    static void RegisterObject(Context* context);
    static Vector<Ship*> ships_;

    Ship(Context* context);
    ~Ship();

    void Start() override;
    void Set(const Vector3& position, const Quaternion& rotation) override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void ApplyMovement(float timeStep);

    void HandleSetControlled() override;
    void EnterPlay(StringHash eventType, VariantMap& eventData) override;
    void EnterLobby(StringHash eventType, VariantMap& eventData) override;

    void GrabPickup(PickupType pickup);
    void PowerupWeapons();
    void PowerupShield();
    void SetHealth(float health);
    float GetHealth() const { return health_; }
    void Hit(float damage, bool melee);
    void Eject();

    void Think() override;
    int GetColorSet() const { return colorSet_; }
    int GetWeaponLevel() const { return weaponLevel_; }

    bool HasAbility(int ability) const;
    bool CanUseAbility(int ability) const;
    void AddRam()   { hasRam_   = true; AddModule(RAM); }
    void AddDive()  { hasDive_  = true; AddModule(DIVE); }
    void AddMines() { hasMines_ = true; AddModule(DEPTHCHARGE); }
    void AddRepel() { hasRepel_ = true; AddModule(REPEL); }
    void RemoveAbilities();
    void AddModule(int ability);

    void EndRam();
    bool IsRamming() const { return ramming_; }
    bool IsDiving() const { return diving_; }

    GUI3D* gui3d_;

protected:
    bool HandleAction(int actionId) override;
    void Blink(const Vector3& newPosition, const Vector3& flashOffset = Vector3::ZERO) override;

private:
    void Ram();
    void Dive();
    void DropDepthCharge();
    void Repel();

    void RemoveTails();
    void CreateTails();
    void Shoot(const Vector3& aim);
    void FireBullet(const Vector3& direction);
    void MoveMuzzle();
    void PlayPickupSample(int pickupCount);
    void Explode();
    void SetTailsEnabled(bool enabled);
    void PickupChaoBall();
    void SetColors();

    void HandleDived(StringHash eventType, VariantMap& eventData);
    void EndDive();

    bool initialized_;
    Vector3 initialPosition_;
    Quaternion initialRotation_;
    int colorSet_;

    const float initialHealth_;
    float health_;

    int weaponLevel_;
    int bulletAmount_;
    const float initialShotInterval_;
    float shotInterval_;
    float sinceLastShot_;

    bool hasRam_;
    bool hasDive_;
    bool hasMines_;
    bool hasRepel_;
    bool ramming_;
    bool diving_;

    int appleCount_;
    int heartCount_;

    Muzzle* muzzle_;
    ParticleEmitter* shineEmitter_;
    Vector<Trail*> tailGens_;
    Node* shieldNode_;
    StaticModel* shieldModel_;
    SharedPtr<Material> shieldMaterial_;

    DiveBubbles* diveBubbles_;
};

DRY_EVENT(E_DIVED, Dived)
{
}

#endif // SHIP_H
