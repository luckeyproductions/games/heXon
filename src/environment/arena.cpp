/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"

#include "tile.h"
#include "../pickup/pickup.h"
#include "../fx/explosion.h"

#include "arena.h"

PODVector<Pair<Vector3, float> > Arena::effectVector_{};

void Arena::RegisterObject(Context *context)
{
    context->RegisterFactory<Arena>();
}

Arena::Arena(Context* context): LogicComponent(context),
    targetPosition_{ Vector3::UP * .666f },
    targetScale_{ Vector3::ONE * .05f },
    hexAffectors_{},
    forceField_{ new Image(context) },
    sinceFieldUpdate_{ M_MAX_FLOAT }
{
    forceField_->SetSize(IntVector2( 17, ARENA_SIZE), 4);
    for (int x{ 0 }; x < forceField_->GetWidth(); ++x)
    for (int y{ 0 }; y < forceField_->GetHeight(); ++y)
    {
        const IntVector2 point{ x, y };
        forceField_->SetPixel(point, Color::TRANSPARENT_BLACK);
    }

    SetUpdateEventMask(USE_UPDATE | USE_FIXEDPOSTUPDATE);
}

void Arena::Start()
{
    node_->SetPosition(targetPosition_);
    node_->SetScale(targetScale_);

    //Create hexagonal field
    //Lays a field of hexagons at the origin
    int bigHexSize{ ARENA_SIZE };

    for (int i{ 0 }; i < bigHexSize; ++i)
    {
        for (int j{ 0 }; j < bigHexSize; ++j)
        {
            if (i < (bigHexSize - bigHexSize / 4) + j / 2 &&                            //Exclude bottom right
                i > (bigHexSize / 4) - (j + 1) / 2 &&                                   //Exclude bottom left
                i + 1 < (bigHexSize - bigHexSize / 4) + ((bigHexSize - j + 1)) / 2 &&   //Exclude top right
                i - 1 > (bigHexSize / 4) - ((bigHexSize - j + 2) / 2))                  //Exclude top left
            {

                Vector3 tilePos{ (-bigHexSize / 2.f + i) * 2.f + j % 2,
                                 -.1f,
                                 (-bigHexSize / 2.f + j) * 1.8f};

                Node* tileNode{ node_->CreateChild("Tile", LOCAL) };
                tileNode->SetPosition(tilePos);
                tiles_.Push(tileNode->CreateComponent<Tile>());
            }
        }
    }

    //Add a directional light to the arena.
    Node* lightNode{ node_->CreateChild("Sun", LOCAL) };
    lightNode->SetPosition(Vector3::UP * 5.f);
    lightNode->SetRotation(Quaternion{ 90.f, 0.f, 0.f });
    playLight_ = lightNode->CreateComponent<Light>();
    playLight_->SetLightType(LIGHT_DIRECTIONAL);
    playLight_->SetBrightness(1.1f);
    playLight_->SetRange(11.f);
    playLight_->SetColor(Color{ 1.f, .9f, .95f });
    playLight_->SetCastShadows(false);
    playLight_->SetEnabled(false);

    //Create heXon logo
    logoNode_ = node_->CreateChild("heXon", LOCAL);
    logoNode_->SetPosition(Vector3{ 0.f, -4.f, 0.f });
    logoNode_->SetRotation(Quaternion{ 0.f, 180.f, 0.f });
    logoNode_->SetScale(16.f);
    StaticModel* logoModel{ logoNode_->CreateComponent<StaticModel>() };
    logoModel->SetModel(RES(Model, "Models/heXon.mdl"));
    logoMaterial_ = RES(Material, "Materials/Loglow.xml");
    xMaterial_ = RES(Material, "Materials/X.xml");
    logoModel->SetMaterial(0, logoMaterial_);
    logoModel->SetMaterial(1, xMaterial_);

//    SubscribeToEvent(E_STATECHANGED, DRY_HANDLER(Arena, HandleGameStateChanged));

    SubscribeToEvent(E_ENTERLOBBY, DRY_HANDLER(Arena, EnterLobby));
    SubscribeToEvent(E_ENTERPLAY,  DRY_HANDLER(Arena, EnterPlay));

//    SubscribeToEvent(E_POSTUPDATE,  DRY_HANDLER(Arena, UpdateEffectVector));
}

void Arena::AddToAffectors(Node* affector)
{
    if (!hexAffectors_.Contains(affector))
        hexAffectors_.Insert(affector);
}

void Arena::RemoveFromAffectors(Node* affector)
{
    if (hexAffectors_.Contains(affector) )
        hexAffectors_.Erase(affector);
}

void Arena::UpdateEffectVector(StringHash, VariantMap&)
{


//    effectVector_.Clear();

//    for (Node* node: hexAffectors_)
//    {
//        if (node->IsEnabled())
//        {
//            Pair<Vector3, float> pair{};
//            pair.first_ = node->GetWorldPosition();
//            pair.second_ = node->GetComponent<RigidBody>()->GetMass();

//            if (node->HasComponent<Explosion>())
//                pair.second_ *= 1.23f;

//            pair.second_ = sqrt(pair.second_);

//            effectVector_.Push(pair);
//        }
//    }
}

void Arena::UpdateMassField()
{
    for (int x{ 0 }; x < forceField_->GetWidth(); ++x)
    for (int y{ 0 }; y < forceField_->GetHeight(); ++y)
    {
        const IntVector2 point{ x, y };
        const Vector3 projectedPoint{ 2.f * targetScale_ * ARENA_SIZE
                    * (Vector3{ (x + .5f) / forceField_->GetWidth(),
                                0.f,
                                (y + .5f) / forceField_->GetHeight() }
                       - Vector3{ .5f, 0.f, .5f }) };

        for (Node* node: hexAffectors_)
        {
            if (node && node->IsEnabled())
            {
                for (int i{ -1 }; i < (node->GetWorldPosition().Length() > ARENA_RADIUS * .666f ? 6 : 0); ++i)
                {
                    const Vector3 projectedPos{ node->GetWorldPosition() + (Quaternion(i * 60.f, Vector3::UP) * Vector3::FORWARD * ARENA_RADIUS * (i == -1 ? 0.f : 2.f)) };

                    if (projectedPos.Length() < 1.333f * ARENA_RADIUS )
                    {
                        RigidBody* body{ node->GetComponent<RigidBody>() };
                        Vector2 flatPos{ projectedPoint.x_ - projectedPos.x_,
                                         projectedPoint.z_ - projectedPos.z_ };
                        const float distance{ flatPos.Length() + PowN(Max(0.f, Abs(projectedPos.y_ * .23f) - 1.f) * 2.f, 2) };
                        float mass{ body->GetMass() };
                        const float distanceFactor{ 1.f - PowN(Clamp((distance - mass * .17f) / mass * .55f, 0.f, 1.f), 3) };

                        float pull{ 1.5f * Abs(mass) * distanceFactor };

                        if (pull != 0.f)
                        {
                            const float mult{ (node->HasComponent<Explosion>() || node->GetDerivedComponent<Pickup>()) ? -1.f : 1.f };
                            const float coreFactor{ Clamp(distance - 1.f, .333f * mult, 1.f) };
                            float ripple{ mult * (.23f * distance + mass * 2.3f)};

                            if (ripple)
                                pull *= MC->Sine(.8f, 1.f - .5f * distanceFactor * coreFactor, coreFactor, ripple);

                            flatPos *= mult * Min( 1.f, pull * distance);

                            Color col{ forceField_->GetPixel(point) };
                            const Vector3 oldVec{ ColorToVector(col) };
                            forceField_->SetPixel(point, VectorToColor(VectorMax(oldVec, Vector3{ flatPos.x_, pull, flatPos.y_ })));
                        }
                    }
                }
            }
        }

        Color col{ forceField_->GetPixel(point) };

        if ( col.a_ != 0.f)
        {
            col.a_ = col.a_ < .01f ? 0.f
                                   : Lerp(col.a_, ALPHASCALE_INV * Max(0.f, Sqrt(ALPHASCALE * col.a_) - 1.f), sinceFieldUpdate_);
            forceField_->SetPixel(point, col);
        }
    }

    sinceFieldUpdate_ = 0.f;
}

Vector3 Arena::ForceFieldAt(const Vector3& position)
{
    const Vector3 scaledVec{ position / (targetScale_ * ARENA_SIZE * 2.f) };
    const Vector2 uv{ scaledVec.x_ + .5f,
                      scaledVec.z_ + .5f };

    return ColorToVector(forceField_->GetPixelBilinear(uv));
}


void Arena::EnterPlay(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    targetPosition_ = Vector3::DOWN * .23f;
    targetScale_ = Vector3::ONE;

    for (Tile* t: tiles_)
        t->lastOffsetY_ = 2.3f;

    playLight_->SetEnabled(true);

    for (int x{ 0 }; x < forceField_->GetWidth(); ++x)
    for (int y{ 0 }; y < forceField_->GetHeight(); ++y)
    {
        forceField_->SetPixel({ x, y }, { Random(1.f), Random(1.f), Random(1.f), Random(1.f) });
    }
}

void Arena::EnterLobby(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    targetPosition_ = Vector3::UP  * .35f;
    targetScale_    = Vector3::ONE * .05f;

    playLight_->SetEnabled(false);
}

void Arena::Update(float timeStep)
{
    const float lerpFactor{ MC->GetGameState() == GS_LOBBY ? 13.f : 6.66f };
    const float t{ Min(1.f, timeStep * lerpFactor) };

    node_->SetPosition(node_->GetPosition().Lerp(targetPosition_, t));
    node_->SetScale(node_->GetScale().Lerp(targetScale_, pow(t, .88f) ));

    logoNode_->SetPosition(logoNode_->GetPosition().Lerp(MC->GetGameState() == GS_LOBBY
                                                         ? Vector3::UP *  4.f * MC->Sine(.666f, .23f, 1.23f)
                                                         : Vector3::UP * -4.f, t));
    logoMaterial_->SetShaderParameter("MatDiffColor", logoMaterial_->GetShaderParameter("MatDiffColor").GetColor().Lerp(
                                          MC->GetGameState() == GS_LOBBY
                                          ? Color{ .42f, Random(.666f), Random(.666f), 2.f } * MC->Sine(5.f, .5f, .8f, .23f)
                                          : Color{ .0666f, .16f, .16f, .23f }, t));
    logoMaterial_->SetShaderParameter("MatEmissiveColor", logoMaterial_->GetShaderParameter("MatEmissiveColor").GetColor().Lerp(
                                          MC->GetGameState() == GS_LOBBY
                                          ? Color{ Random(.42f), Random(.42f), Random(.42f) } * MC->Sine(4.f, 1.3f, 2.3f, .23f)
                                          : Color{ .005f, .05f, .02f }, t));
    xMaterial_->SetShaderParameter("MatDiffColor", MC->GetGameState() == GS_LOBBY
                                          ? logoMaterial_->GetShaderParameter("MatDiffColor").GetColor()
                                          : xMaterial_->GetShaderParameter("MatDiffColor").GetColor().Lerp(Color{ .0666f, .16f, .16f, .23f }, t));
    xMaterial_->SetShaderParameter("MatEmissiveColor", MC->GetGameState() == GS_LOBBY
                                          ? logoMaterial_->GetShaderParameter("MatEmissiveColor").GetColor()
                                          : xMaterial_->GetShaderParameter("MatEmissiveColor").GetColor().Lerp(Color{ .005f, .05f, .02f }, t));

}

void Arena::FixedPostUpdate(float timeStep)
{
    if (GetScene()->IsUpdateEnabled() && sinceFieldUpdate_ > timeStep * 23.f)
        UpdateMassField();
    else
        sinceFieldUpdate_ += timeStep;
}

Tile* Arena::GetRandomTile(bool forMason)
{
    if (tiles_.Size())
    {
        Tile* tile{ nullptr };

        while (!tile)
        {

            Tile* tryTile{ tiles_[Random() * tiles_.Size()] };

            if (forMason)
            {
                for (float angle: { 0.f, 60.f, 120.f })
                {
                    Vector3 axis{ Quaternion{ angle, Vector3::UP } * Vector3::FORWARD };
                    float distanceToCenter{ Abs(tryTile->GetNode()->GetWorldPosition().ProjectOntoAxis(axis)) };

                    if ( distanceToCenter < 1.f || distanceToCenter > 19.f)
                        continue;
                }
            }

//            if (tryTile->IsFree()) {
                tile = tryTile;
//            }
        }

        return tile;
    }

    return nullptr;
}

void Arena::FlashX(const Color& color)
{
    xMaterial_->SetShaderParameter("MatEmissiveColor", color * 5.f);
}
