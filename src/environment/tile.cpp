/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"

#include "arena.h"
#include "tile.h"

StaticModelGroup* Tile::tileGroup_{ nullptr };

void Tile::RegisterObject(Context* context)
{
    context->RegisterFactory<Tile>();
}

Tile::Tile(Context* context): LogicComponent(context),
    lastOffsetY_{ .666f },
    flipped_{ RandomBool() }
{
    SetUpdateEventMask(USE_UPDATE);
}

void Tile::Start()
{
    if (!tileGroup_)
    {
        tileGroup_ = GetScene()->CreateComponent<StaticModelGroup>();
        tileGroup_->SetModel(RES(Model, "Models/Hexagon.mdl"));
        tileGroup_->SetMaterial(RES(Material, "Materials/BackgroundTile.xml"));
        tileGroup_->SetCastShadows(false);
    }

    node_->SetScale(1.1f);
    tileGroup_->AddInstanceNode(node_);
//    model_ = node_->CreateComponent<StaticModel>();
//    model_->SetModel(RES(Model, "Models/Hexagon.mdl"));
//    model_->SetMaterial(RES(Material, "Materials/BackgroundTile.xml"));
//    model_->SetCastShadows(false);

    referencePosition_ = node_->GetPosition();
    centerDistExp_ = exp2(.75f * referencePosition_.Length());
}

void Tile::Update(float timeStep)
{
    const float elapsedTime{ GetScene()->GetElapsedTime() };
    float offsetY{ 0.f };

    //Switch curcuit
    if (Random(23) == 5)
        node_->SetRotation(Quaternion{ Random(3) * 120.f + 60.f * flipped_, Vector3::UP });

    //Calculate periodic tile movement

//    for (Pair<Vector3, float> hexAffector: Arena::GetEffectVector())
//    {
    const float offsetYPart{ GetSubsystem<Arena>()->ForceFieldAt(referencePosition_).y_ };//hexAffector.second_ - (0.1f * referencePosition_.DistanceToPoint(hexAffector.first_)) };
    wave_ = PowN(Sine(offsetYPart + Abs(centerDistExp_ - elapsedTime * 5.2625f)), 4);

//    Log::Write(LOG_INFO, referencePosition_.ToString() + " -> " + String{ offsetYPart });

    offsetY = offsetYPart;
//    offsetY = sqrt(offsetY);
    offsetY += wave_ * .17f;
    offsetY = Lerp(lastOffsetY_, offsetY, Min(1.f, timeStep * 9.f));
    lastOffsetY_ = offsetY;

    node_->SetPosition(referencePosition_ + Min(offsetY, 4.f) * Vector3::DOWN);

//    bool lobby{ MC->GetGameState() == GS_LOBBY };
//    float brightness{ Clamp((0.23f * offsetY) + 0.25f, 0.0f, 1.0f) + 0.42f * static_cast<float>(lobby) };
//    Color color{ brightness + offsetY * lobby,
//                 brightness + offsetY * 0.00042f * (MC->Sine(23.0f, -23.0f - 1000.0f * lobby, 23.0f + 1000.0f * lobby, 23.0f) * wave_),
//                 brightness - Random(0.23f) * lobby, brightness + (0.023f * wave_) };
//    model_->GetMaterial(0)->SetShaderParameter("MatDiffColor", color);
}
