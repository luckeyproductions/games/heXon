/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PANEL_H
#define PANEL_H


#include "../luckey.h"

class Panel: public LogicComponent
{
    DRY_OBJECT(Panel, LogicComponent);

public:
    Panel(Context* context);
    static void RegisterObject(Context* context);
    void Initialize(int colorSet);
    void Update(float timeStep) override;

    void ActivatePanel(StringHash eventType, VariantMap& eventData);
    void DeactivatePanel(StringHash eventType, VariantMap& eventData);

    void EnterPlay(StringHash eventType, VariantMap& eventData);
    void EnterLobby(StringHash eventType, VariantMap& eventData);

    bool HasOwner();
    bool IsOwner(int playerId);

private:
    void FadeInPanel();
    void FadeOutPanel(bool immediate = false);
    void CreatePanels();
    void SetCurrentInfoNode(Node* infoNode);
    void CreateInfos();

    Vector<Node*> infoNodes_;
    Node* currentInfoNode_;
    unsigned currentInfoNodeIndex_;
    float sinceInfoChange_;
    bool active_;
    int colorSet_;

    Scene* panelScene_;
    SharedPtr<Texture2D> panelTexture_;
    Node* panelTriggerNode_;
    Node* smallPanelNode_;
    Node* bigPanelNode_;
};

#endif // PANEL_H
