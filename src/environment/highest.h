/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef HIGHEST_H
#define HIGHEST_H


#include "../luckey.h"

class Pilot;

static const Vector3 HIGHEST_POS{ 0.f, 2.3f, -3.4f };

class Highest: public LogicComponent
{
    DRY_OBJECT(Highest, LogicComponent);

public:
    static void RegisterObject(Context* context);
    Highest(Context* context);

    void Start() override;
    void Update(float timeStep);
    void SetPilot(Pilot* pilot, unsigned score);

    void EnterLobby(StringHash eventType, VariantMap& eventData);
    void EnterPlay(StringHash eventType, VariantMap& eventData);
    void SetScore(unsigned score);

    void Fade(bool out);

    static Highest* Get() { return instance_; }

private:
    static Highest* instance_;
    unsigned highestScore_;
    Text* highestScoreText_;
    Pilot* highestPilot_;
};

#endif // HIGHEST_H
