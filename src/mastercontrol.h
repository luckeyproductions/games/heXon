/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

namespace Dry {

/// Game state changed event
DRY_EVENT(E_STATECHANGED, StateChanged)
{
    DRY_PARAM(P_STATE, State); // GameState
    DRY_PARAM(P_LASTSTATE, LastState); // Previous GameState
}
DRY_EVENT(E_ENTERPLAY, EnterPlay) {}
DRY_EVENT(E_ENTERLOBBY, EnterLobby) {}

class Node;
class Scene;
}

class Player;
class Ship;
class heXoCam;
class InputMaster;
class Arena;
class SpawnMaster;
class Razor;
//class Player;
class Door;
class SplatterPillar;
class Apple;
class ChaoBall;
class Heart;
class Lobby;
class Settings;

typedef struct GameWorld
{
    SharedPtr<heXoCam> camera;
    float lastReset;
    SharedPtr<Octree> octree;
    SharedPtr<Node> backdropNode;
    SharedPtr<Node> voidNode;
    struct {
        SharedPtr<Node> sceneCursor;
        SharedPtr<Cursor> uiCursor;
        PODVector<RayQueryResult> hitResults;
    } cursor;
} GameWorld;

typedef struct ColorSet
{
    Pair<Color, Color> colors_;
    SharedPtr<Material> glowMaterial_;
    SharedPtr<Material> hullMaterial_;
    SharedPtr<Material> bulletMaterial_;
    SharedPtr<Material> repelMaterial_;
    SharedPtr<Material> panelMaterial_;
    SharedPtr<Material> addMaterial_;
    SharedPtr<ParticleEffect> hitFx_;
} ColorSet;

typedef struct HitInfo
{
    Vector3 position_;
    Vector3 hitNormal_;
    Node* hitNode_;
    Drawable* drawable_;
} HitInfo;

enum GameState { GS_INTRO, GS_LOBBY, GS_PLAY, GS_DEAD, GS_EDIT };

#define MC GetSubsystem<MasterControl>()
#define NAVMESH MC->scene_->GetComponent<NavigationMesh>()

class MasterControl : public Application
{
    DRY_OBJECT(MasterControl, Application);

public:
    MasterControl(Context* context);
    String GetStoragePath() const { return storagePath_; }

    void Setup() override;
    void Start() override;
    void Stop()  override;
    void Exit();

    Sound* GetMusic(const String& name) const;
    Sound* GetSample(const String& name);

    void AddPlayer();
    void RemoveAutoPilot();
    Player* GetPlayer(int playerId) const;
    Player* GetPlayerByColorSet(int colorSet);
    Player* GetNearestPlayer(const Vector3& pos);
    Vector<SharedPtr<Player> > GetPlayers();
    unsigned NumPlayers() const { return players_.Size(); }
    void RemovePlayer(Player* player);
    bool AllReady(bool onlyHuman);
    bool AllPlayersScoreZero(bool onlyHuman);

    float SinceLastReset() const { return scene_->GetElapsedTime() - world.lastReset; }
    void SetGameState(GameState newState);
    GameState GetGameState(){ return currentState_; }
    void PlayBossMusic() { musicSource_->Play(bossMusic_); }
    float AspectRatio() const { return 1.f * GRAPHICS->GetWidth() / GRAPHICS->GetHeight(); }
    bool IsPaused() { return paused_; }
    void SetPaused(bool paused);
    void Pause()   { SetPaused(true);  }
    void Unpause() { SetPaused(false); }
    float GetSinceStateChange() const noexcept { return sinceStateChange_; }

    bool PhysicsRayCast(PODVector<PhysicsRaycastResult> &hitResults, const Ray& ray, const float distance, const unsigned collisionMask = M_MAX_UNSIGNED);
    bool PhysicsRayCastSingle(PhysicsRaycastResult& hitResult, const Ray& ray, const float distance, const unsigned collisionMask = M_MAX_UNSIGNED);
    bool PhysicsSphereCast(PODVector<RigidBody*> &hitResults, const Vector3& center, const float radius, const unsigned collisionMask = M_MAX_UNSIGNED);

    void Eject();
    bool NoHumans();
    void LoadHighest();

    float Sine(const float freq, const float min, const float max, const float shift = .0f);
    float Cosine(const float freq, const float min, const float max, const float shift = .0f);
    bool InsideHexagon(const Vector3& position, float radius) const;
    Vector3 GetHexant(const Vector3& position) const;

    Ship* GetShipByColorSet(int colorSet);

    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);
    void HandleJoystickConnected(StringHash eventType, VariantMap& eventData);

    template <class T> PODVector<T*> GetComponentsInScene(bool onlyEnabled = true)
    {
        PODVector<Node*> matchingNodes;
        scene_->GetChildrenWithComponent<T>(matchingNodes, true);

        PODVector<T*> matchingComponents{};
        for (Node* n: matchingNodes)
        {
            if (!onlyEnabled || n->IsEnabled())
                matchingComponents.Push(n->GetDerivedComponent<T>());
        }

        return matchingComponents;
    }

    template <class T> T* GetNearest(const Vector3& location, bool onlyEnabled = true)
    {
        T* nearest{ nullptr };

        PODVector<T*> components{};
        scene_->GetComponents<T>(components, true);

        for (T* c: components)
        {
            if (!nearest)
            {
                nearest = c;
                continue;
            }

            Node* nearestNode{ static_cast<Component*>(nearest)->GetNode() };
            Node* node{        static_cast<Component*>(c)      ->GetNode() };

            if (onlyEnabled && !node->IsEnabled())
                continue;

            const float nearestDistance{ nearestNode->GetWorldPosition().DistanceToPoint(location) };
            const float distance{               node->GetWorldPosition().DistanceToPoint(location) };

            if (nearestDistance > distance)
                nearest = c;
        }

        return nearest;
    }

    GameWorld world;
    Scene* scene_;
    PhysicsWorld* physicsWorld_;
    SoundSource* musicSource_;
    Lobby* lobby_;
    Arena* arena_;

    Vector< SharedPtr<Player> > players_;
    HashMap< int, ColorSet > colorSets_;

    Apple* apple_;
    Heart* heart_;
    ChaoBall* chaoBall_;

private:
    void CreateColorSets();
    void CreateScene();
    void CreateUI();
    void SubscribeToEvents();

    void HandleUpdate(StringHash, VariantMap &eventData);
    void UpdateCursor(const float timeStep);

    void LeaveGameState();
    void EnterGameState();

    float SinePhase(float freq, float shift);

    String storagePath_;

    bool paused_;
    GameState currentState_;
    float sinceStateChange_;

    SharedPtr<Sound> menuMusic_;
    SharedPtr<Sound> gameMusic_;
    SharedPtr<Sound> bossMusic_;

    float secondsPerFrame_;
    float sinceFrameRateReport_;
};

#endif // MASTERCONTROL_H
