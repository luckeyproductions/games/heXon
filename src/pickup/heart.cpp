/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../fx/mirage.h"

#include "heart.h"

void Heart::RegisterObject(Context* context)
{
    context->RegisterFactory<Heart>();
}

Heart::Heart(Context* context): Pickup(context)
{
}

void Heart::Start()
{
    Pickup::Start();

    node_->SetName("Heart");
    pickupType_ = PT_HEART;
    initialPosition_ = Vector3::BACK * 10.8f;
    node_->SetPosition(initialPosition_);
    model_->SetModel(RES(Model, "Models/Heart.mdl"));
    model_->SetMaterial(RES(Material, "Materials/RedEnvmap.xml"));

    Vector<ColorFrame> colorFrames{};
    colorFrames.Push(ColorFrame(Color(.0f , .0f , .0f , .0f) ,  .0f));
    colorFrames.Push(ColorFrame(Color(.75f, .23f, .23f, .42f),  .23f));
    colorFrames.Push(ColorFrame(Color(.0f , .0f , .0f , .0f) ,  .42f));
    colorFrames.Push(ColorFrame(Color(.75f, .23f, .23f, .75f),  .75f));
    colorFrames.Push(ColorFrame(Color(.0f , .0f , .0f , .0f) , 1.2f));
    particleEmitter_->GetEffect()->SetColorFrames(colorFrames);

    graphicsNode_->CreateComponent<Mirage>()->SetColor(Color{ .7f, .23f, .1f, .75f });
}

void Heart::Update(float timeStep)
{
    Pickup::Update(timeStep);

    //Spin
    graphicsNode_->Rotate(Quaternion(0.f, 100.f * timeStep, 0.f));
    //Float like a float
    const float floatFactor{ .5f - Min(.5f, .5f * Abs(node_->GetPosition().y_)) };
    graphicsNode_->SetPosition(Vector3::UP * MC->Sine(.23f, -floatFactor, floatFactor));
}
