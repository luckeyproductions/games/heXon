/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../inputmaster.h"
#include "../environment/arena.h"
#include "../fx/hitfx.h"
#include "../player/player.h"
#include "../player/ship.h"

#include "pickup.h"

Pickup::Pickup(Context* context): SceneObject(context)
{
    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void Pickup::Start()
{
    SceneObject::Start();

    node_->AddTag("Pickup");
    MC->arena_->AddToAffectors(node_);
    graphicsNode_ = node_->CreateChild("Graphics");

    model_ = graphicsNode_->CreateComponent<StaticModel>();
    node_->SetScale(.8f);
    node_->SetEnabledRecursive(false);

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetRestitution(.666f);
    rigidBody_->SetMass(.666f);
    rigidBody_->SetLinearFactor(Vector3::ONE - Vector3::UP);
    rigidBody_->SetLinearDamping(.5f);
    rigidBody_->SetFriction(0.f);
    rigidBody_->SetAngularFactor(Vector3::ZERO);
    rigidBody_->SetAngularDamping(0.f);
    rigidBody_->ApplyTorque(Vector3::UP * 32.f);
    rigidBody_->SetLinearRestThreshold(0.f);
    rigidBody_->SetAngularRestThreshold(0.f);
    rigidBody_->SetCollisionLayerAndMask(4, M_MAX_UNSIGNED - 1);

    CollisionShape* collisionShape = node_->CreateComponent<CollisionShape>();
    collisionShape->SetSphere(2.3f);

    triggerNode_ = node_->CreateChild("PickupTrigger");
    triggerBody_ = triggerNode_->CreateComponent<RigidBody>();
    triggerBody_->SetTrigger(true);
    triggerBody_->SetKinematic(true);
    triggerBody_->SetCollisionLayerAndMask(1, 1);

    CollisionShape* triggerShape{ triggerNode_->CreateComponent<CollisionShape>() };
    triggerShape->SetSphere(2.5f);

    SubscribeToEvent(triggerNode_, E_NODECOLLISION, DRY_HANDLER(Pickup, HandleNodeCollision));

    particleEmitter_ = graphicsNode_->CreateComponent<ParticleEmitter>();
    particleEmitter_->SetEffect(CACHE->GetTempResource<ParticleEffect>("Particles/Shine.xml"));
}

void Pickup::Update(float timeStep)
{
    Emerge(timeStep);
}

void Pickup::FixedUpdate(float timeStep)
{
    // Update linear damping
    if (!IsEmerged())
    {
        rigidBody_->SetLinearDamping(Min(1.f, .5f - node_->GetPosition().y_ * .42f));
    }
    else
    {
        if (rigidBody_->GetLinearDamping() != .5f)
            rigidBody_->SetLinearDamping(.5f);

    }

    const Vector3 fieldVector{ GetSubsystem<Arena>()->ForceFieldAt(node_->GetWorldPosition()) };

    if (fieldVector.LengthSquared() != 0.f)
    {
        const Vector3 forceVector{ -23.f * fieldVector.y_ * fieldVector.ProjectOntoPlane(Vector3::UP) };

        rigidBody_->ApplyForce(timeStep * forceVector * Max(0.f, 1.f - Abs(node_->GetPosition().y_)));
    }
}

void Pickup::HandleNodeCollision(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!node_->IsEnabled() || node_->GetWorldPosition().y_ < -17.f)
        return;

    PODVector<RigidBody*> collidingBodies;
    triggerBody_->GetCollidingBodies(collidingBodies);
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    if (!otherNode)
        return;

    if (otherNode->HasComponent<Ship>())
    {
        Ship* ship{ otherNode->GetComponent<Ship>() };
        if (!ship->IsDiving())
        {
            ship->GrabPickup(pickupType_);
            SPAWN->Create<HitFX>()
                    ->Set(GetWorldPosition(), ship->GetColorSet(), false);

            switch (pickupType_)
            {
            case PT_CHAOBALL: SPAWN->ChaoPickup(); Disable(); break;
            case PT_APPLE: case PT_HEART: Respawn(); break;
            default: break;
            }
        }
    }
}

void Pickup::Set(const Vector3& position)
{
    SceneObject::Set(position);

    SubscribeToEvent(triggerNode_, E_NODECOLLISIONSTART, DRY_HANDLER(Pickup, HandleNodeCollision));
}

void Pickup::Respawn(bool restart)
{
    rigidBody_->SetLinearVelocity(Vector3::ZERO);
    rigidBody_->ResetForces();

    Set(restart ? initialPosition_ : SPAWN->SpawnPoint());
}
