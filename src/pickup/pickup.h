/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PICKUP_H
#define PICKUP_H

#include "../sceneobject.h"

class Pickup: public SceneObject
{
    DRY_OBJECT(Pickup, SceneObject);

public:
    Pickup(Context* context);

    void Start() override;

    void Set(const Vector3& position) override;
    void Respawn(bool restart = false);
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;

    PickupType GetPickupType() { return pickupType_; }

    void HandleNodeCollision(StringHash eventType, VariantMap& eventData);

protected:

    PickupType pickupType_;
    Vector3 initialPosition_;
    Node* triggerNode_;
    RigidBody* rigidBody_;
    RigidBody* triggerBody_;
    StaticModel* model_;
    ParticleEmitter* particleEmitter_;
};

#endif // PICKUP_H
