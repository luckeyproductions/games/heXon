/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../fx/hitfx.h"
#include "../player/ship.h"
#include "../player/player.h"

#include "coin.h"

Vector<Coin*> Coin::coins_{};
StaticModelGroup* Coin::coinGroup_{};

void Coin::RegisterObject(Context* context)
{
    context->RegisterFactory<Coin>();
    context->MC->GetSample("Coin");
}

Coin::Coin(Context* context): SceneObject(context),
    bubbleEmitter_{}
{
    big_ = false;

    SetUpdateEventMask(USE_UPDATE);
}

void Coin::Start()
{
    bubbleEmitter_ = node_->CreateChild("Bubbles")->CreateComponent<ParticleEmitter>();
    bubbleEmitter_->SetEffect(RES(ParticleEffect, "Particles/Bubbles.xml"));

    if (!coinGroup_)
    {
        coinGroup_ = MC->scene_->CreateComponent<StaticModelGroup>();
        coinGroup_->SetModel(RES(Model, "Models/Coin.mdl"));
        coinGroup_->SetMaterial(RES(Material, "Materials/ChromeEnvmap.xml"));
    }

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(.42f);
    rigidBody_->SetLinearDamping(.88f);
    rigidBody_->SetAngularDamping(.13f);
    rigidBody_->SetTrigger(true);

    CollisionShape* collisionShape{ node_->CreateComponent<CollisionShape>() };
    collisionShape->SetSphere(.75f);
}

void Coin::Set(const Vector3& position)
{
//    node_->GetComponent<ParticleEmitter>()->RemoveAllParticles();
    bubbleEmitter_->SetEmitting(true);

    rigidBody_->SetLinearVelocity(Vector3::ZERO);
    rigidBody_->SetAngularVelocity(Vector3::ZERO);
    rigidBody_->ResetForces();
    SceneObject::Set(position);
    node_->Translate(Vector3{ Random(-.42f, .42f), 1.f, Random(-.42f, .42f) });
    node_->SetRotation(Quaternion::IDENTITY);
    rigidBody_->ApplyImpulse((GetWorldPosition() - position).Normalized());
    rigidBody_->ApplyTorqueImpulse(Vector3{ RandomOffCenter(.034f, .05f),
                                            Random(-.005f, .005f),
                                            RandomOffCenter(.023f, .06f) });
    coinGroup_->AddInstanceNode(node_);
    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Coin, HandleNodeCollisionStart));

    coins_.Push(this);
}

void Coin::Disable()
{
    coinGroup_->RemoveInstanceNode(node_);
    SceneObject::Disable();

    bubbleEmitter_->GetNode()->SetEnabled(true);
    bubbleEmitter_->SetEmitting(false);

    coins_.Remove(this);
}

void Coin::Update(float)
{
    if (GetWorldPosition().y_ < -23.f)
        Disable();

    ParticleEffect* bubbles{ bubbleEmitter_->GetEffect() };
    bubbles->SetMinEmissionRate(Max(0.f, Min(GetWorldPosition().y_ + 2.3f, 2.3f * (rigidBody_->GetLinearVelocity().Length() - 3.f))));
    bubbles->SetMaxEmissionRate(Max(0.f, Min(GetWorldPosition().y_ + 4.2f, 2.3f * (rigidBody_->GetLinearVelocity().Length() - 2.f))));
}

void Coin::HandleNodeCollisionStart(StringHash /*eventType*/, VariantMap& eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    if (!otherNode)
        return;

    if (otherNode->HasComponent<Ship>())
    {
        Ship* ship{ otherNode->GetComponent<Ship>() };
        ship->GetPlayer()->AddScore(10);
        ship->PlaySample(MC->GetSample("Coin"), .12f);

        SPAWN->Create<HitFX>()->Set(GetWorldPosition(), ship->GetColorSet());
        Disable();
    }
}

void Coin::Launch()
{
    rigidBody_->ApplyImpulse(Vector3::UP * 17.f);
}
