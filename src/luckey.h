/*
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once

#include <Dry/Audio/Audio.h>
#include <Dry/Audio/AudioEvents.h>
#include <Dry/Audio/Sound.h>
#include <Dry/Audio/SoundListener.h>
#include <Dry/Audio/SoundSource.h>
#include <Dry/Audio/SoundSource3D.h>
#include <Dry/Container/HashBase.h>
#include <Dry/Container/HashMap.h>
#include <Dry/Container/Vector.h>
#include <Dry/Core/CoreEvents.h>
#include <Dry/Core/Object.h>
#include <Dry/Core/Spline.h>
#include <Dry/Engine/Application.h>
#include <Dry/Engine/Console.h>
#include <Dry/Engine/DebugHud.h>
#include <Dry/Engine/Engine.h>
#include <Dry/Engine/EngineDefs.h>
#include <Dry/Graphics/AnimatedModel.h>
#include <Dry/Graphics/AnimationController.h>
#include <Dry/Graphics/Animation.h>
#include <Dry/Graphics/AnimationState.h>
#include <Dry/Graphics/BillboardSet.h>
#include <Dry/Graphics/Camera.h>
#include <Dry/Graphics/DebugRenderer.h>
#include <Dry/Graphics/DecalSet.h>
#include <Dry/Graphics/Graphics.h>
#include <Dry/Graphics/GraphicsEvents.h>
#include <Dry/Graphics/Light.h>
#include <Dry/Graphics/Material.h>
#include <Dry/Graphics/Model.h>
#include <Dry/Graphics/Octree.h>
#include <Dry/Graphics/OctreeQuery.h>
#include <Dry/Graphics/ParticleEffect.h>
#include <Dry/Graphics/ParticleEmitter.h>
#include <Dry/Graphics/Renderer.h>
#include <Dry/Graphics/RenderPath.h>
#include <Dry/Graphics/RibbonTrail.h>
#include <Dry/Graphics/Skybox.h>
#include <Dry/Graphics/StaticModel.h>
#include <Dry/Graphics/StaticModelGroup.h>
#include <Dry/Graphics/Texture2D.h>
#include <Dry/Graphics/Viewport.h>
#include <Dry/Graphics/Zone.h>
#include <Dry/Input/InputEvents.h>
#include <Dry/Input/Input.h>
#include <Dry/IO/FileSystem.h>
#include <Dry/IK/IK.h>
#include <Dry/IK/IKSolver.h>
#include <Dry/IK/IKEffector.h>
#include <Dry/IO/Log.h>
#include <Dry/IO/MemoryBuffer.h>
#include <Dry/Math/MathDefs.h>
#include <Dry/Math/Plane.h>
#include <Dry/Math/Sphere.h>
#include <Dry/Math/Vector2.h>
#include <Dry/Math/Vector3.h>
#include <Dry/Navigation/NavigationMesh.h>
#include <Dry/Navigation/Navigable.h>
#include <Dry/Network/Network.h>
#include <Dry/Physics/CollisionShape.h>
#include <Dry/Physics/Constraint.h>
#include <Dry/Physics/PhysicsEvents.h>
#include <Dry/Physics/PhysicsWorld.h>
#include <Dry/Physics/RigidBody.h>
#include <Dry/Resource/ResourceCache.h>
#include <Dry/Resource/XMLFile.h>
#include <Dry/Scene/LogicComponent.h>
#include <Dry/Scene/Component.h>
#include <Dry/Scene/Node.h>
#include <Dry/Scene/SceneEvents.h>
#include <Dry/Scene/Scene.h>
#include <Dry/Scene/ObjectAnimation.h>
#include <Dry/UI/Font.h>
#include <Dry/UI/Text.h>
#include <Dry/UI/UI.h>
#include <Dry/UI/UIEvents.h>
#include <Dry/UI/Button.h>
#include <Dry/UI/CheckBox.h>
#include <Dry/UI/DropDownList.h>
#include <Dry/UI/ListView.h>
#include <Dry/UI/Slider.h>
#include <Dry/UI/Window.h>
#include <Dry/Scene/ValueAnimation.h>

#include <Dry/DebugNew.h>

#include <initializer_list>

#define FILES GetSubsystem<FileSystem>()
#define TIME GetSubsystem<Time>()
#define CACHE GetSubsystem<ResourceCache>()
#define INPUT GetSubsystem<Input>()
#define GRAPHICS GetSubsystem<Graphics>()
#define RENDERER GetSubsystem<Renderer>()
#define AUDIO GetSubsystem<Audio>()
#define SPAWN GetSubsystem<SpawnMaster>()

#define RES(x, y) GetSubsystem<ResourceCache>()->GetResource<x>(y)
#define FONT_RABBIT RES(Font, "Fonts/WhiteRabbitReloaded.ttf")

constexpr int Layer(int x) { return 1 << (x - 1); }
enum PickupType { PT_RESET = 0, PT_APPLE, PT_HEART, PT_CHAOBALL };

using namespace Dry;
namespace LucKey {

unsigned IntVector2ToHash(IntVector2 vec);
Vector3 PolarPoint(float radius, float angle);
float Delta(float lhs, float rhs, bool angle = false);
Vector2 Rotate(const Vector2 vec2, const float angle);
Color RandomColor();
Color RandomSkinColor();
Color RandomHairColor(bool onlyNatural = false);

float Sine(float x);
float Cosine(float x);

int Cycle(int x, int min, int max);
float Cycle(float x, float min, float max);
}
using namespace LucKey;
