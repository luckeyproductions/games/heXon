/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "menumaster.h"
#include "../settings.h"

#include "settingspage.h"

SettingsPage::SettingsPage(Context* context): Window(context),
    settings_{},
    currentResolutionText_{ nullptr }
{
    SetAlignment(HA_CENTER, VA_TOP);
    SetSize(640, 640);
    SetPosition(0, 272);
    SetColor(Color::TRANSPARENT_BLACK);
}

void SettingsPage::AddSettings(const Vector<Setting>& settings)
{
    for (const Setting& s: settings)
        CreateSetting(s);

    // Position rows
    int y{ 32 };
    for (UIElement* row: children_)
    {
        row->SetPosition(0, y);
        y += row->GetHeight();
    }
}

PODVector<UIElement*> SettingsPage::GetFocusableElements() const
{
    PODVector<UIElement*> focusChildren{};
    PODVector<UIElement*> children{};
    GetChildren(children, true);
    for (UIElement* e: children)
    {
        if (e->GetFocusMode() != FM_NOTFOCUSABLE && e->IsVisibleEffective())
            focusChildren.Push(e);
    }

    return focusChildren;
}

void SettingsPage::CreateSetting(const Setting& setting)
{
    UIElement* row{ CreateChild<UIElement>() };
    row->SetAlignment(HA_CENTER, VA_TOP);
    row->SetHeight(96);

    Text* text{ row->CreateChild<Text>() };
    text->SetFont(FONT_RABBIT);
    text->SetFontSize(42);
    text->SetColor(Color::AZURE);
    text->SetAlignment(HA_LEFT, VA_CENTER);
    text->SetPosition(-278, 0);
    text->SetTextEffect(TE_STROKE);
    text->SetEffectColor(Color::CYAN.Transparent(.7f));
    text->SetOpacity(.7f);
    text->SetText(setting.name_);

    switch (setting.value_.GetType())
    {
    default: return;
    case VAR_BOOL:
    {
        CheckBox* box{ MenuMaster::CreateCheckbox(row) };
        box->SetVar("Setting", setting.name_);
        box->SetChecked(setting.value_.GetBool());

        SubscribeToEvent(box, E_TOGGLED, DRY_HANDLER(SettingsPage, HandleValueChanged));
    }
        break;
    case VAR_FLOAT:
    {
        text->Remove();
        const float val{ setting.value_.GetFloat() };

        Slider* slider{ row->CreateChild<Slider>() };
        slider->SetVar("Setting", setting.name_);
        slider->SetBlendMode(BLEND_ALPHA);
        slider->SetAlignment(HA_CENTER, VA_CENTER);
        slider->SetSize(512, 96);
        slider->SetTexture(RES(Texture2D, "UI/CheckBox.png"));
        slider->SetImageRect({ 0, 0, 128, 128 });
        slider->SetImageBorder({ 48, 0, 48, 0 });
        slider->SetBorder({ 35, 0, 35, 0 });
        slider->SetRange(4.2f);
        slider->SetValue(slider->GetRange() * val);
        slider->SetFocusMode(FM_FOCUSABLE);

        BorderImage* knob{ slider->GetKnob() };
        knob->SetBlendMode(BLEND_ALPHA);
        knob->SetTexture(RES(Texture2D, "UI/CheckBox.png"));
        knob->SetImageRect({ 0, 0, 128, 128 });

        SubscribeToEvent(slider, E_SLIDERCHANGED, DRY_HANDLER(SettingsPage, HandleValueChanged));
        SubscribeToEvent(slider, E_FOCUSED, DRY_HANDLER(SettingsPage, HandleSliderFocusChanged));
        SubscribeToEvent(slider, E_DEFOCUSED, DRY_HANDLER(SettingsPage, HandleSliderFocusChanged));
    }
        break;
    case VAR_INTVECTOR2:
    {
        Button* resolutionButton{ row->CreateChild<Button>() };       
        resolutionButton->SetAlignment(HA_CENTER, VA_TOP);
//        resolutionButton->SetBlendMode(BLEND_ALPHA);
        resolutionButton->SetStyleAuto(RES(XMLFile, "UI/Style.xml"));
        resolutionButton->SetSize(512, 96);

        const IntVector2 val{ setting.value_.GetIntVector2() };
        text->SetText(String{ val.x_ } + "x" + String{ val.y_ });
        text->SetAlignment(HA_CENTER, VA_CENTER);
        text->SetPosition(0, 0);
        resolutionButton->AddChild(text);
        currentResolutionText_ = text;

        ListView* resolutionList{ resolutionButton->CreateChild<ListView>() };
        resolutionList->SetStyleAuto(RES(XMLFile, "UI/Style.xml"));
        resolutionList->SetScrollBarsAutoVisible(false);
        resolutionList->SetScrollBarsVisible(false, false);
        resolutionList->SetFocusMode(FM_NOTFOCUSABLE);
        resolutionList->SetAlignment(HA_CENTER, VA_BOTTOM);
        resolutionList->SetSize(512, 512);
        resolutionList->SetPosition(0, -row->GetHeight());
        resolutionList->SetVisible(false);
        resolutionButton->SetVar("Toggle", { resolutionList });

        BorderImage* contentElem{ static_cast<BorderImage*>(resolutionList->GetScrollPanel()) };
        contentElem->SetBlendMode(BLEND_ADDALPHA);
        contentElem->SetHorizontalAlignment(HA_CENTER);
        contentElem->SetColor(Color::BLACK.Lerp(Color::WHITE, .23f));

        HashSet<IntVector2> resolutions{};
        for (const IntVector3& mode: GRAPHICS->GetResolutions(0))
            resolutions.Insert({ mode.x_, mode.y_ });

        for (const IntVector2& res: resolutions)
        {
            Button* resButton{ CreateChild<Button>() };
            resButton->SetVar("Setting", setting.name_);
            resButton->SetVar("Value", res);
            resButton->SetStyleAuto(RES(XMLFile, "UI/Style.xml"));
            resButton->SetHorizontalAlignment(HA_CENTER);
            resButton->SetMinSize(420, 96);
            Text* resolutionText{ resButton->CreateChild<Text>() };
            resolutionText->SetAlignment(HA_CENTER, VA_CENTER);
            resolutionText->SetFont(FONT_RABBIT, 42);
            resolutionText->SetColor(Color::AZURE);
            resolutionText->SetTextEffect(TE_STROKE);
            resolutionText->SetEffectColor(Color::CYAN.Transparent(.7f));
            resolutionText->SetOpacity(.7f);
            resolutionText->SetText(String{ res.x_ } + "x" + String{ res.y_ });

            resolutionList->AddItem(resButton);
            resButton->SetVar("Toggle", { resolutionList });
            SubscribeToEvent(resButton, E_PRESSED, DRY_HANDLER(SettingsPage, HandleResolutionButtonPressed));

            if (resolutionText->GetText() == text->GetText())
                resButton->SetVisible(false);
        }

//        SharedPtr<XMLFile> xmlFile{ MakeShared<XMLFile>(context_) };
//        XMLElement xmlRoot{ xmlFile->CreateRoot("element") };
//        resolutionList->SaveXML(xmlRoot);
//        xmlFile->SaveFile(MC->GetStoragePath() + "/UI/ResolutionPicker.xml");

        SubscribeToEvent(resolutionButton, E_PRESSED, DRY_HANDLER(SettingsPage, HandleResolutionButtonPressed));
    }
        break;
    }
}

void SettingsPage::HandleResolutionButtonPressed(StringHash /*eventType*/, VariantMap& eventData)
{
    Button* pressedButton{ static_cast<Button*>(eventData[ClickEnd::P_ELEMENT].GetPtr()) };
    pressedButton->SetHovering(false);
    const Variant toggleVar{ pressedButton->GetVar("Toggle") };
    if (!toggleVar.IsEmpty())
    {
        ListView* toggleList{ static_cast<ListView*>(toggleVar.GetPtr()) };
        toggleList->SetVisible(!toggleList->IsVisible());

        for (UIElement* child: GetChildren())
        {
            UIElement* firstChild{ child->GetChild(0) };
            if (toggleList->GetParent() != firstChild)
                child->SetVisible(!toggleList->IsVisible());
        }
    }
    const Variant valVar{ pressedButton->GetVar("Value") };
    if (!valVar.IsEmpty())
    {
        const IntVector2 resolution{ valVar.GetIntVector2() };
        if (resolution != GRAPHICS->GetSize())
        {
            for (UIElement* e: pressedButton->GetParent()->GetChildren())
                e->SetVisible(e != pressedButton);

            currentResolutionText_->SetText(String{ resolution.x_ } + "x" + String{ resolution.y_ });
            currentResolutionText_->GetParent()->SetVar("Value", resolution);

            GetSubsystem<Settings>()->SetSetting(pressedButton->GetVar("Setting").GetString(),
                                                 pressedButton->GetVar("Value"));
        }
    }
}

void SettingsPage::HandleValueChanged(StringHash eventType, VariantMap& eventData)
{
    UIElement* elem{ static_cast<UIElement*>(eventData[Click::P_ELEMENT].GetPtr()) };
    const String settingName{ elem->GetVar("Setting").GetString() };

    Settings* settings{ GetSubsystem<Settings>() };
    if (eventType == E_TOGGLED)
        settings->SetSetting(settingName, eventData[Toggled::P_STATE]);
    else if (eventType == E_SLIDERCHANGED)
    {
        const float range{ static_cast<Slider*>(elem)->GetRange() };
        settings->SetSetting(settingName, { eventData[SliderChanged::P_VALUE].GetFloat() / range });
    }
//    else if (eventType == E_CLICKEND && !elem->GetVar("Value").IsEmpty())
//        settings->SetSetting(settingName, elem->GetVar("Value"));
}

void SettingsPage::HandleSliderFocusChanged(StringHash eventType, VariantMap& eventData)
{
    Slider* slider{ static_cast<Slider*>(eventData[Focused::P_ELEMENT].GetPtr()) };
    const bool focus{ slider->HasFocus() };
    BorderImage* knob{ slider->GetKnob() };
    knob->SetImageRect({ 0, 128 * focus, 128, 128 * (1 + focus) });
}
